import unittest

import qutip
import numpy as np
import scipy.special

from functions import (heterodyne, effective_squeezing)

class TestFunctions(unittest.TestCase):

    def test_heterodyne(self):
        """Test that a heterodyne measurement returns the correct alpha as most likely case"""
        xvec = np.linspace(-5, 5, 1001)
        N = 10
        for alpha in [0 + 1j*0, 1 + 1j*0, 0 + 1j, 1 + 1j]:
            psi = qutip.coherent(N, alpha / np.sqrt(2))
            out = heterodyne(psi, xvec, xvec, method='max')

            self.assertAlmostEqual(out[0], alpha.real)
            self.assertAlmostEqual(out[1], alpha.imag)

    def test_effective_squeezing(self):
        """Test effective Squeezing using a squeezed state"""
        N = 500
        delta = 0.3
        spacing = np.sqrt(2*np.pi)

        S_p = qutip.displace(N, spacing/np.sqrt(2))
        S_q = qutip.displace(N, 1j*spacing/np.sqrt(2))

        r = np.log(1 / delta)
        psi = qutip.squeeze(N, r) * qutip.coherent(N, 0)
        d = effective_squeezing(psi, [S_p, S_q], spacing)
        dp = d[0]
        dq = d[1]
        self.assertAlmostEqual(dp, 1/delta)
        self.assertAlmostEqual(dq, delta)

    def test_effective_squeezing_GKP(self):
        """Test effective Squeezing using a GKP state"""
        N = 500
        delta = 0.3
        spacing = np.sqrt(2*np.pi)

        S_p = qutip.displace(N, spacing/np.sqrt(2))
        S_q = qutip.displace(N, 1j*spacing/np.sqrt(2))

        r = np.log(1 / delta)
        psi = qutip.squeeze(N, r) * qutip.coherent(N, 0)

        envelope = []
        for j in range(5):
            envelope.append(scipy.special.binom(4, j) * S_p**j)
        envelope = sum(envelope)
        envelope *= S_p.dag()**2

        gkp = (envelope * psi).unit()

        d = effective_squeezing(gkp, [S_p, S_q], spacing)
        dp = d[0]
        dq = d[1]
        print(d)
        self.assertAlmostEqual(dp, 0.3769052856, 4)
        self.assertAlmostEqual(dq, delta, 4)