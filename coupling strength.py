#%%
import numpy as np
import scipy.constants as const
import scimath
import matplotlib.pyplot as plt

from scimath.units.SI import kilogram, henry, coulomb, second, meter, joule, hertz, farad

J = joule
F = farad
kg = kilogram
H = henry
C = coulomb
s = second
m = meter
GHz = 10**9 * hertz

#%%
const.physical_constants["Planck constant"]
h = const.physical_constants["Planck constant"][0] * J *s

#%%
const.physical_constants["elementary charge"]
e = const.physical_constants["elementary charge"][0] * C

#%%
def inductive_energy(inductance):
    res = h**2/(4 * 4 * np.pi**2 * e**2 * inductance)
    if type(res) == scimath.units.unit:
        assert res.derivation == J.derivation, res
    return res

def Inductance(energy):
    res = h**2/(4 * 4 * np.pi**2 * e**2 * energy)
    assert res.derivation == H.derivation, res
    return res

def eff_inductive_energy(inductance, EJ, x_ext):
    return inductive_energy(inductance) - EJ * np.cos(x_ext)

def charging_energy(capacitance):
    res = e**2/(2 * capacitance)
    assert res.derivation == J.derivation, res
    return res

def Capacitance(ec):
    res = e**2/(2 * ec)
    assert res.derivation == F.derivation, res
    return res

def EC_freq(freq, inductive_energy):
    res = freq ** 2 * h**2/ ( 8 * inductive_energy)
    assert res.derivation == J.derivation, res
    return res

def Frequency(energy):
    res = energy/h
    if type(res) == scimath.units.unit:
        assert res.derivation == (0, 0, -1, 0, 0, 0, 0), res
    return res

def Energy(frequency):
    res = frequency * h
    if type(res) == scimath.units.unit:
        assert res.derivation == (2, 1, -2, 0, 0, 0, 0), res
    return res

#%%
def xi(ec, el):
    result = (
        2 * ec
        / el
        )**(1/4)
    return result


#%%
EJ_freq = np.arange(5,45,5) * GHz
EJ = Energy(EJ_freq)

#%%
x_ext = np.pi/2
g = []
g3 = []
L = []
EL = []
C_T = []
C_A = []
ECT = []
ECA = []
kerr = []
kerrT = []
ckerr = []
dfA = []
dfT = []
for ej in EJ:
    L_T = Inductance(ej) / 10
    L_A = Inductance(ej) / 10
    EL.append(inductive_energy(L_A))
    EL_T = eff_inductive_energy(L_T, ej, x_ext)
    EL_A = eff_inductive_energy(L_A, ej, x_ext)
    EC_T = EC_freq(0.5 * GHz, EL_T)
    ECT.append(EC_T)
    EC_A = EC_freq(10 * GHz, EL_A)
    ECA.append(EC_A)

    fmaxT = np.sqrt(8*eff_inductive_energy(L_T, ej, np.pi)*EC_T/J**2)/h * J
    fminT = np.sqrt(8*eff_inductive_energy(L_T, ej, 0)*EC_T/J**2)/h * J
    dfT.append(fmaxT-fminT)
    fmaxA = np.sqrt(8*eff_inductive_energy(L_A, ej, np.pi)*EC_A/J**2)/h * J
    fminA = np.sqrt(8*eff_inductive_energy(L_A, ej, 0)*EC_A/J**2)/h * J
    dfA.append(fmaxA-fminA)

    res = ej * xi(EC_A, EL_A)**2 * xi(EC_T, EL_T)
    res3 = ej * xi(EC_T, EL_T)**3
    gamma = xi(EC_T, EL_T)**2/(3*xi(EC_A, EL_A)**2)
    kerr.append(ej * xi(EC_A, EL_A)**4)
    kerrT.append(ej * xi(EC_T, EL_T)**4)
    ckerr.append(ej * xi(EC_T, EL_T)**2 *xi(EC_A, EL_A)**2)
    L.append(L_T)
    C_T.append(Capacitance(EC_T))
    C_A.append(Capacitance(EC_A))
    g.append(res)
    g3.append(res3)

g = np.array(g)
g3 = np.array(g3)
kerr = np.array(kerr)
kerrT = np.array(kerrT)
L = np.array(L)
C_T = np.array(C_T)
C_A = np.array(C_A)
ckerr = np.array(ckerr)

#%%
plt.plot(np.array(dfA)*1000/GHz)

#%%
plt.plot(np.array(dfT)*1000/GHz)

#%%
plt.plot(Frequency(np.array(EL))/(GHz))

#%%
plt.plot(10**9*L/H)

#%%
plt.plot(Frequency(np.array(ECT))*1000/(GHz))
#%%
plt.plot(Frequency(np.array(ECA))*1000/(GHz))


#%%
plt.plot(Frequency(g)*1000/GHz)
plt.plot(Frequency(g3)*1000/GHz)
#plt.plot(g3/g)

#%%
plt.plot(Frequency(kerr)*1000/GHz)

#%%
plt.plot(Frequency(g)/Frequency(kerr))

#%%
plt.plot(Frequency(g)/Frequency(ckerr))

#%%
plt.plot(Frequency(g)/Frequency(kerrT))

#%%
ej = Energy(10 * GHz)
hm = []
gm = []
for scale in np.arange(1,10,0.5):
    L_T = Inductance(ej) / (scale*10)
    L_A = Inductance(ej) / 20
    EL_T = eff_inductive_energy(L_T, ej, x_ext)
    EL_A = eff_inductive_energy(L_A, ej, x_ext)
    EC_T = EC_freq(0.5 * GHz, EL_T)
    EC_A = EC_freq(10 * GHz, EL_A)
    res = ej * xi(EC_A, EL_A)**2 * xi(EC_T, EL_T)
    res3 = ej * xi(EC_T, EL_T)**3
    gamma = xi(EC_T, EL_T)**2/(3*xi(EC_A, EL_A)**2)
    hm.append(gamma)
    gm.append(res)

plt.plot(hm)
plt.plot(hm[0]/np.arange(1,10,0.5))

#%%
gm = np.array(gm)
plt.plot(Frequency(gm)*1000/GHz)
plt.plot(Frequency(gm[0])*1000/GHz/np.sqrt(np.arange(1,10,0.5)))
#%%
plt.plot(g3/g)

#%%
plt.plot(C_T*10**12/F)
#%%
plt.plot(C_A*10**12/F)

#%%
plt.plot(L*10**9/H)


#%%
plt.plot(EJ_freq/GHz)

#%%
np.sqrt(2*np.pi)/(15*GHz/1000)


#%%
np.sqrt(2*np.pi)/(3*GHz/1000)

#%%
print(Capacitance(Energy(0.3*GHz))*10**12/F)

#%%

print(Frequency(charging_energy(50*10**(-12)*F))*1000/GHz)
print(Frequency(charging_energy(1000*10**(-12)*F))*1000/GHz)

#%%
np.sqrt(8*400*0.00002)

#%%
