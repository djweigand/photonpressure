import concurrent.futures
import functools
import pickle
import time
import warnings
from typing import List, Tuple

import matplotlib
import matplotlib.pyplot as plt
import numpy as np
import qutip
from mpl_toolkits.axes_grid1 import make_axes_locatable
from qutip import (basis, coherent, destroy, displace, expect,
                   ket2dm, liouvillian, mcsolve, mesolve, operator_to_vector,
                   plot_fock_distribution, qeye, qfunc, squeeze, tensor,
                   thermal_dm, vector_to_operator, wigner, wigner_cmap)

import functions
from functions import (effective_squeezing, heterodyne, post_measurement,
                       qfunc_amat, System, Params, low_priority)

if __name__ == "__main__":
    low_priority()

    matplotlib.rcParams['mathtext.fontset'] = 'cm'
    matplotlib.rc('text', usetex=True)


    START_TIME = time.time()

    warnings.warn("There is a bug in MKL crashing this script, use other BLAS versions of numpy/scipy. Check version using np.__config__.show()")
    import os
    #data_dir = 'data/'
    data_dir = 'python/data/'
    assert os.path.exists(data_dir)

    plot=True

    """Settings """
    purity = False
    photons = False

    plot = "Kerr"
    debug_figs = False
    filetype = "pdf"

    load_data = False
    save_data = True

    nr_samples = 200
    n_range = np.arange(1,4.5,0.5)
    #n_range = np.arange(1,2,0.5)

    """System parameters """
    params = Params('GKP')
    params.nr_samples = nr_samples
    params.n_range = n_range
    #params.dims = (500, 20)
    if plot == "DeltaQ_estimate_vac":
        params.measurement = 'random'
        params.initial_T = ('coherent', 0)
        data_name_end = "_vac"
        noise_types = (None,)
    elif plot == "DeltaQ_Noise_sq3":
        params.measurement = 'random'
        params.initial_T = ('squeezed', 3)
        data_name_end = "_sq3"
        noise_types = (None, "q3", "q3_adag3_a3")
    elif plot == "DeltaQ_PL_vac":
        params.measurement = 'random'
        params.method = 'MC2'
        params.ntraj = 1000
        params.initial_T = ('coherent', 0)
        #data_name_end = "_vac"
        data_name_end = "_vac_pl_1_2"
        noise_types = (None,)
        # The loss rate is set as fraction of the coupling strength, order is Target, Oscillator
        # If the coupling strength is 10MHz, assume that kappa_T = 10 kHz, kappa_A = 30 kHz
        params.kappa = (10**4/10**7, 10**4/10**7)
    elif plot == "Kerr":
        params.measurement = 'fixed'
        params.method = 'Lindblad'
        params.initial_T = ('coherent', 0)
        noise_types = (None,)

        params.time_dependent = True
        params.kerr = 1.
        drive_freq0 = 2*np.pi * 2 / (2 * np.sqrt(np.pi))
        params.drive_freq = drive_freq0 * 0.5
        
        data_name_end = "_vac_kerr"
    else: raise NotImplementedError

    if save_data is True:
        print(str(noise_types)+data_name_end)
        ok = input("Overwriting saved data. Type \"OK\" to continue.")
        assert ok == "OK"
    elif load_data == False and save_data == False:
        ok = input("Generating new data without saving. Type \"OK\" to continue.")
        assert ok == "OK"

    for q3 in noise_types:
        params.q3 = q3

        dq = []
        dp = []
        dq_err = []
        dp_err = []
        full_q = []
        full_p = []

        if load_data:
            with open(data_dir+str(q3)+data_name_end, "rb") as file:
                data = pickle.load(file)
        else:
            data = {"params": params}
            dat = []
            for n in n_range:
                params.ancilla_photons = n
                """Initialize """
                system = System(params)
                print("Squeezing", system.effective_squeezing())

                """Simulate """
                print("n=", n, "time=", time.time()-START_TIME)
                if plot == "Kerr":
                    system.update('q')
                    print("meas done, time=", time.time()-START_TIME)
                    q, p = system.result
                    na = system.photons()[0]
                    delta_p, delta_q = system.effective_squeezing() # pylint: disable=unbalanced-tuple-unpacking
                    out = [[delta_p, delta_q, na, (q+ 1j*p)/np.sqrt(2)],[delta_p, delta_q, na, (q+ 1j*p)/np.sqrt(2)]]
                else:
                    system.pre_measurement('q')
                    print("pre meas done, time=", time.time()-START_TIME)
                    with concurrent.futures.ProcessPoolExecutor(max_workers=6) as executor:
                        out = list(executor.map(system.sample, range(nr_samples)))
    #                    out = list(map(system.sample, range(nr_samples)))
                dat.append(out)
            data["data"] = np.real_if_close(np.array(dat))
            print(data["data"].shape)
            print(data["data"])

            if save_data:
                with open(data_dir+str(q3)+data_name_end, "wb") as file:
                    pickle.dump(data, file, protocol=-1)

        if plot:
            dat = data["data"]
            x = data["params"].n_range

            mean = np.mean(dat, axis=1)
            std = np.std(dat, axis=1)

            fig, ax = plt.subplots(1, 1, figsize=(3.4, 3.4))
            if plot == "DeltaQ_estimate_vac":
                labels = [r'$\langle\Delta_q\rangle$',
                            r"Villain",
                            r'$(4\pi |\alpha|^2)^{-1/2}$',
                            r'$\left(16\pi^2 |\alpha|^2(1+|\alpha|^2)\right)^{-1/4}$']
                #colors = plt.cm.inferno(np.linspace(0,1,4))
                #colors = plt.cm.cividis(np.linspace(0,1,4))
                #colors = plt.cm.magma(np.linspace(0,1,4))
                #colors = plt.cm.plasma(np.linspace(0,1,4))
                colors = plt.cm.viridis(np.linspace(0,1,4)) # pylint: disable=no-member
                (line1,_,_) = plt.errorbar(x, mean[:,1], yerr=std[:,1],color=colors[0],label="1")
                x_analytic = np.arange(1,10.5,0.5)
                (line3,) = ax.plot(x_analytic, 1/np.sqrt(4*np.pi*x_analytic),color=colors[2],label="2")
                (line4,) = ax.plot(x_analytic,
                    1/np.sqrt(4*np.pi*np.sqrt(x_analytic * np.sqrt(1+x_analytic**2))),
                        color=colors[3],label="3")
                y_estimate = np.array((0.452203, 0.33867, 0.273943, 0.232666, 0.204477, 0.184209, 0.168998, 0.15715, 0.147623, 0.139751, 0.133099, 0.12737, 0.122361, 0.117925, 0.113956, 0.110374, 0.107117, 0.104138, 0.101397))
                (line2,) = ax.plot(x_analytic, y_estimate, color=colors[1],label="4")
                ax.set_ylim(0,0.5)
                plt.legend((line1, line2, line3, line4), labels)
            elif plot in ("DeltaQ_Noise_sq3", "DeltaQ_PL_vac", "Kerr"):
                x_analytic = np.arange(1,10.5,0.5)
                ones = np.ones_like(x_analytic)
                ax.errorbar(x, mean[:,1], yerr=std[:,1], label=r'$\Delta_q$')
                ax.errorbar(x, mean[:,0], yerr=std[:,0], label=r'$\Delta_p$')
                ax.plot(x_analytic, ones)
                #ax.set_xlabel(r'$|\alpha|^2$')
                ax.set_ylim(0,0.5)
                plt.legend()
            fig.tight_layout()
            if plot is True:
                fig.savefig(f"figures/squeezing_n_{str(q3)}{data_name_end}.{filetype}",bbox_inches='tight', pad_inches=0)
            elif plot == "DeltaQ_Noise_sq3":
                fig.savefig(f"figures/{plot}_{str(q3)}.{filetype}",bbox_inches='tight', pad_inches=0)
            else:
                fig.savefig(f"figures/{str(plot)}.{filetype}",bbox_inches='tight', pad_inches=0)
        if debug_figs:
            x = data["params"].n_range
            dat_t = dat.transpose()

            # fig1, ax1 = plt.subplots()
            # ax1.boxplot(dat_t[1,:,:], positions=x, whis=[5, 95])
            # fig1.tight_layout()
            # fig1.savefig(f"figures/squeezing vs n whisker q - {str(q3)}.{filetype}")

            fig, ax = plt.subplots(1, 1, figsize=(3.4, 3.4))
            ax.boxplot(dat_t[1,:,:], positions=x, whis=[5, 95])
            ax.set_xlabel(r'$\bar{n}_A$')
            ax.set_ylabel(r'$\Delta_q$')
            fig.tight_layout()
            fig.savefig(f"figures/whisker_q_{str(q3)}{data_name_end}.{filetype}",bbox_inches='tight', pad_inches=0)

            fig, ax = plt.subplots(1, 1, figsize=(3.4, 3.4))
            ax.boxplot(dat_t[0,:,:], positions=x, whis=[5, 95])
            ax.set_xlabel(r'$\bar{n}_A$')
            ax.set_ylabel(r'$\Delta_p$')
            fig.tight_layout()
            fig.savefig(f"figures/whisker_p_{str(q3)}{data_name_end}.{filetype}",bbox_inches='tight', pad_inches=0)

            fig, ax = plt.subplots(1, 1, figsize=(3.4, 3.4))
            ax.boxplot(dat_t[2,:,:], positions=x, whis=[5, 95])
            ax.set_xlabel(r'$\bar{n}_A$')
            ax.set_ylabel(r'$\bar{n}_T$')
            fig.tight_layout()
            fig.savefig(f"figures/photons_{str(q3)}{data_name_end}.{filetype}",bbox_inches='tight', pad_inches=0)

            # fig, ax = plt.subplots()
            # ax.boxplot(np.abs(dat_t[3,:,:]), positions=x)
            # fig.tight_layout()
            # fig.savefig(f"figures/beta vs n - {str(q3)}.{filetype}")

            # fig, ax = plt.subplots()
            # ax.boxplot(np.angle(dat_t[3,:,:]), positions=x)
            # fig.tight_layout()
            # fig.savefig(f"figures/phi vs n - {str(q3)}.{filetype}")
