"""Animate the time evolution during photonpressure coupling for talks

Uses only the ideal Hamiltonian, with various initial states for the target mode
("vac", "sq3", "gkp", "sensor", "sq03d", "sq03dm", 'sq03'):
1. Vacuum
2. Anti-Squeezed vac (p=0 eigenstate)
3. GKP state
4. Sensor state
5. Sq. Vac, Displaced to right by D(\sqrt{2\pi})
6. Sq. Vac, Displaced to left by D(-\sqrt{2\pi})
7. Sq. Vac (q eigenstate)

Raises:
    NotImplementedError: sensor state might be broken, fix before use
"""
import time

import matplotlib
import matplotlib.pyplot as plt
import numpy as np
import qutip

from functions import System, Params

from matplotlib.transforms import TransformedBbox, Affine2D
import matplotlib.animation as animation
import os
import psutil

process = psutil.Process(os.getpid())
process.nice(psutil.IDLE_PRIORITY_CLASS)
process.ionice(psutil.IOPRIO_VERYLOW)

if __name__ == "__main__":
    font_size = 30

    os.chdir("talk/figures/")
    matplotlib.rcParams['mathtext.fontset'] = 'cm'
    matplotlib.rc('text', usetex=True)
    matplotlib.rcParams.update({'font.size': font_size})
    matplotlib.rcParams['mathtext.fontset'] = 'cm'
    matplotlib.rcParams['axes.labelsize'] = font_size
    matplotlib.rc('text', usetex=True)
    matplotlib.rcParams['xtick.labelsize'] = font_size
    matplotlib.rcParams['ytick.labelsize'] = font_size
    matplotlib.rcParams['legend.fontsize'] = font_size

    START_TIME = time.time()

    #data_dir = 'data/'

    # """Settings """
    purity = False

    # """System parameters """
    params = Params("sensor")
    displacement_mul = 1
    params.ancilla_photons = 3
    params.method = 'Lindblad'
    #params['unconditional'] = False

    params.w_range = 6
    params.n_points = 201
    params.t_steps = 50

    params.dims = (150, 15)

    params.measurement = 'fixed'

    for plot in ("vac", "sq3", "gkp", "sq03d", "sq03dm", 'sq03'):
    #for plot in ("sq03dm",):
        if plot == "vac":
            params.initial_T = ('coherent', 0)
        elif plot == "sq3":
            params.initial_T = ('squeezed', 3)
        elif plot == 'gkp':
            N = params.dims[0]
            delta = 0.3
            r = np.log(1 / delta)
            sq_vac = qutip.squeeze(N, r) * qutip.basis(N, 0)
            gkp = qutip.Qobj(dims=[[N],[1]])
            k = np.arange(-3,3+1)
            for k_ in k:
                gkp += (np.exp(-displacement_mul**2 *np.pi*k_**2 * delta**2) * qutip.displace(N, k_*np.sqrt(displacement_mul*np.pi)) * sq_vac)
            gkp = gkp.unit()
            params.initial_T = ('custom', gkp)
        elif plot == 'sensor':
            raise NotImplementedError
            #params['spacing'] = np.sqrt(2 * np.pi)  # Use a sensor state
            N = params.dims[0]
            delta = 0.3
            r = np.log(1 / delta)
            sq_vac = qutip.squeeze(N, r) * qutip.basis(N, 0)
            sensor = qutip.Qobj(dims=[[N],[1]])
            k = np.arange(-3,3+1)
            for k_ in k:
                sensor += (np.exp(-np.pi*k_**2 * delta**2) * qutip.displace(N, k_*np.sqrt(np.pi)) * sq_vac)
            sensor = sensor.unit()
            params.initial_T = ('custom', sensor)
        elif plot == 'sq03':
            #params['spacing'] = np.sqrt(2 * np.pi)  # Use a sensor state
            N = params.dims[0]
            delta = 0.3
            r = np.log(1 / delta)
            sq_vac = qutip.squeeze(N, r) * qutip.basis(N, 0)
            sq03 = sq_vac.unit()
            params.initial_T = ('custom', sq03)
        elif plot == 'sq03d':
            #params['spacing'] = np.sqrt(2 * np.pi)  # Use a sensor state
            N = params.dims[0]
            delta = 0.3
            r = np.log(1 / delta)
            sq_vac = qutip.squeeze(N, r) * qutip.basis(N, 0)
            sq03d = (qutip.displace(N, np.sqrt(displacement_mul*np.pi)) * sq_vac).unit()
            params.initial_T = ('custom', sq03d)
        elif plot == 'sq03dm':
            #params['spacing'] = np.sqrt(2 * np.pi)  # Use a sensor state
            N = params.dims[0]
            delta = 0.3
            r = np.log(1 / delta)
            sq_vac = qutip.squeeze(N, r) * qutip.basis(N, 0)
            sq03dm = (qutip.displace(N, -np.sqrt(displacement_mul*np.pi)) * sq_vac).unit()
            params.initial_T = ('custom', sq03dm)
        else:
            raise ValueError

        # """Initialize """
        system = System(params)

        # """Simulate """
        out = system.anim('q')

        names = ['target_w', 'target_p_w', 'ancilla_w', 'ancilla_h']
        w_range = params.w_range/np.sqrt(2 * np.pi)

        for i, dat in enumerate(out):
            fig, ax = plt.subplots(1,1)
            ims = []
            m = np.max(dat)
            norm = matplotlib.colors.Normalize(-m, m)
            cmap = plt.get_cmap('seismic_r')
            for d in dat:
                im = ax.imshow(d, animated=True, norm=norm, cmap=cmap, extent=(-w_range, w_range, -w_range, w_range))
                ims.append([im])

            ax.axhline(linewidth=0.1, color='k')
            ax.axvline(linewidth=0.1, color='k')
            ax.set_xlabel(r'$q/\sqrt{2\pi}$')
            ax.set_ylabel(r'$p/\sqrt{2\pi}$')
            ax.set_xticks(np.arange(-2, 3))
            ax.set_yticks(np.arange(-2, 3))

            tight_bbox_raw = ax.get_tightbbox(fig.canvas.get_renderer())
            tight_bbox = TransformedBbox(tight_bbox_raw, Affine2D().scale(1./fig.dpi))

            ani = animation.ArtistAnimation(fig, ims, interval=300, blit=True,
                                    repeat_delay=3000, repeat=False)
            savefig_kwargs = {"pad_inches":0}
            #fig.tight_layout(pad=0.3, w_pad=0, h_pad=0)
            fig.tight_layout(pad=0, w_pad=0, h_pad=0)
            ani.save(f'{plot}_{names[i]}.mp4', savefig_kwargs=savefig_kwargs)
            plt.close(fig)
            #os.system(f"ffmpeg -y -loglevel warning -i {plot}_{names[i]}.mp4 -loop 0 -final_delay 1500 {plot}_{names[i]}.gif")
            os.system(f"ffmpeg -y -loglevel warning -i {plot}_{names[i]}.mp4 -vf \"crop=490:ih:50:0\" {plot}_{names[i]}_crop.mp4")