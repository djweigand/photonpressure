import concurrent.futures
import functools
import pickle
import time
import warnings
from typing import List, Tuple

import matplotlib
import matplotlib.pyplot as plt
import numpy as np
import qutip
from mpl_toolkits.axes_grid1 import make_axes_locatable
from qutip import (basis, coherent, destroy, displace, expect,
                   ket2dm, liouvillian, mcsolve, mesolve, operator_to_vector,
                   plot_fock_distribution, qeye, qfunc, squeeze, tensor,
                   thermal_dm, vector_to_operator, wigner, wigner_cmap)

import functions
from functions import (effective_squeezing, heterodyne, post_measurement,
                       qfunc_amat, System)

import yappi

import os
import psutil

process = psutil.Process(os.getpid())
process.nice(psutil.IDLE_PRIORITY_CLASS)
process.ionice(psutil.IOPRIO_VERYLOW)


matplotlib.rcParams['mathtext.fontset'] = 'cm'
matplotlib.rc('text', usetex=True)


START_TIME = time.time()


if __name__ == "__main__":
    yappi.start()

    warnings.warn("There is a bug in MKL crashing this script, use other BLAS versions of numpy/scipy. Check version using np.__config__.show()")
    """
    #params = {'kappa': [0.2, 2], 'g':1}          # Units in 100 kHz
    params = {'kappa': [0.01, 0.1]}          # Units in 100 kHz
    #params = {'kappa': None}          # Units in 100 kHz
    params['dims'] = (40, 20)
    params['ancilla_photons'] = 5
    params['measurement'] = 'max'
    params['initial'] = ('coherent', 0)
    params['method'] = 'MC'

    print("Start", START_TIME-time.time())
    system = System(params)
    e = np.asarray(system.entropy())
    print("e", START_TIME-time.time())
    print(system.purity())
    system.update('q')
    print("evo1", START_TIME-time.time())
    print(system.purity())
    system.update('p')
    print("evo2", START_TIME-time.time())
    print(system.purity())
    """
    import os
    #data_dir = 'data/'
    data_dir = 'python/data/'
    assert os.path.exists(data_dir)

    plot=True

    to_sim = "line_plot"
    #to_sim = "eff_sq"

    if to_sim == "line_plot":
        """Settings """
        purity = False
        photons = True
        plot = "NoNoise_vac"
        #plot = "NoNoise_sq3"
        #plot = "q3_adag3_a3_sq3"
        #plot = "q3_sq3"

        """System parameters """
        #params = {'kappa': [0.2, 2], 'g':1}          # Units in 100 kHz
        #params = {'kappa': [0.01, 0.4]}          # Units in 100 kHz
        params = {'kappa': None}          # Units in 100 kHz
        params['spacing'] = 2 * np.sqrt(np.pi)  # Use a GKP state
        params['dims'] = (150, 20)
        params['ancilla_photons'] = 3
        #params['measurement'] = 'random'
        params['measurement'] = 'max'
        #params['method'] = 'MC'
        params['method'] = 'Lindblad'

        if plot is False:
            pass
        elif plot == "NoNoise_vac":
            params['initial'] = ('coherent', 0)
            params['q3'] = None
            params['w_range'] = 6
            params['n_points'] = 401
        elif plot == "NoNoise_sq3":
            params['initial'] = ('squeezed', 3)
            params['q3'] = None
            params['w_range'] = 6
            params['n_points'] = 401
        elif plot == "q3_adag3_a3_sq3":
            params['initial'] = ('squeezed', 3)
            params['q3'] = "q3_adag3_a3"
            params['w_range'] = 6
            params['n_points'] = 401
            params['q3_strength'] = 0.003
        elif plot == "q3_sq3":
            params['ancilla_photons'] = 3
            params['initial'] = ('squeezed', 3)
            params['q3'] = "q3"
            params['w_range'] = 6
            params['n_points'] = 401
            params['q3_strength'] = 0.003
        else:
            raise NotImplementedError

        #plot = False
        """Initialize """
        if plot:
            fig3, ax3 = plt.subplots(1, 4, figsize=(12, 4))
        system = System(params)

        """Simulate """
        print("Start", time.time()-START_TIME)
        if purity:
            print("Purity", system.purity())
        if photons:
            print("Photons",system.photons())
        print("Squeezing", system.effective_squeezing())
        if plot:
            system.plot_line(fig3, ax3[0], "Target", wmap=True, title="Target (Initial)")
            system.plot_line(fig3, ax3[1], "Ancilla", wmap=True, title="Ancilla (Initial)")
        print("Evolution Q", time.time()-START_TIME)
        system.update('q')
        if photons:
            print("Photons",system.photons(), "pre disp")
        #system.displace(1j*np.sqrt(2)*params['ancilla_photons'])
        #if photons:
        #    print("Photons",system.photons(), "post disp")
        print("Plot. Time:", time.time()-START_TIME)
        if plot:
            system.plot_line(fig3, ax3[2], "Husimi", wmap=True)
            system.plot_line(fig3, ax3[3], "Target", wmap=True, title="Target (Final)")
        print("Evolution P", time.time()-START_TIME)
        #system.update('p')
        #system.displace(-2)
        print("Plot. Time:", time.time()-START_TIME)
        print("Done", time.time()-START_TIME)
        if purity:
            print("Purity", system.purity())
        if photons:
            print("Photons",system.photons())
        print(system.effective_squeezing())
        if plot:
            fig3.tight_layout(pad=0.3,w_pad=0,h_pad=0)
            fig3.savefig(f'figures/{str(plot)}.pdf',bbox_inches='tight', pad_inches=0)

    if to_sim == "eff_sq":
        """Settings """
        purity = False
        photons = False

        plot = "DeltaQ_estimate_vac"
        #plot = "DeltaQ_Noise_sq3"
        #plot = "DeltaQ_PL_vac"
        debug_figs = False
        filetype = "pdf"

        load_data = True
        save_data = False

        nr_samples = 200
        nrange = np.arange(1,4.5,0.5)
        #nrange = np.arange(1,2,0.5)

        """System parameters """
        params = {}
        params['n_points'] = 401
        params['spacing'] = 2 * np.sqrt(np.pi)  # Use a GKP state
        params['nr_samples'] = nr_samples
        params['nrange'] = nrange
        params['dims'] = (500, 20)
        params['measurement'] = 'random'
        if plot == "DeltaQ_estimate_vac":
            params['method'] = 'Lindblad'
            params['initial'] = ('coherent', 0)
            data_name_end = "_vac"
            noise_types = (None,)
        elif plot == "DeltaQ_Noise_sq3":
            params['method'] = 'Lindblad'
            params['initial'] = ('squeezed', 3)
            data_name_end = "_sq3"
            noise_types = (None, "q3", "q3_adag3_a3")
        elif plot == "DeltaQ_PL_vac":
            params['method'] = 'MC2'
            params['ntraj'] = 1000
            #data_name_end = "_vac"
            data_name_end = "_vac_pl_1_2"
            noise_types = (None,)
            # The loss rate is set as fraction of the coupling strength, order is Target, Oscillator
            # If the coupling strength is 10MHz, assume that kappa_T = 10 kHz, kappa_A = 30 kHz
            params['kappa'] = (10**4/10**7, 10**4/10**7)
        else: raise NotImplementedError

        if save_data is True:
            print(str(noise_types)+data_name_end)
            ok = input("Overwriting saved data. Type \"OK\" to continue.")
            assert ok == "OK"
        elif load_data == False and save_data == False:
            ok = input("Generating new data without saving. Type \"OK\" to continue.")
            assert ok == "OK"

        for q3 in noise_types:
            params['q3'] = q3

            dq = []
            dp = []
            dq_err = []
            dp_err = []
            full_q = []
            full_p = []

            if load_data:
                with open(data_dir+str(q3)+data_name_end, "rb") as file:
                    data = pickle.load(file)
            else:
                data = {"params": params}
                dat = []
                for n in nrange:
                    params['ancilla_photons'] = n
                    """Initialize """
                    system = System(params)
                    print("Squeezing", system.effective_squeezing())

                    """Simulate """
                    print("n=", n, "time=", time.time()-START_TIME)
                    system.pre_measurement('q')
                    print("pre meas done, time=", time.time()-START_TIME)
                    with concurrent.futures.ProcessPoolExecutor(max_workers=6) as executor:
                        out = list(executor.map(system.sample, range(nr_samples)))
#                    out = list(map(system.sample, range(nr_samples)))
                    dat.append(out)
                data["data"] = np.array(dat)
                print(data["data"].shape)

                if save_data:
                    with open(data_dir+str(q3)+data_name_end, "wb") as file:
                        pickle.dump(data, file, protocol=-1)

            if plot:
                dat = data["data"]
                x = data["params"]["nrange"]

                mean = np.mean(dat, axis=1)
                std = np.std(dat, axis=1)

                fig, ax = plt.subplots(1, 1, figsize=(3.4, 3.4))
                if plot == "DeltaQ_estimate_vac":
                    labels = [r'$\langle\Delta_q\rangle$',
                              r"Villain",
                              r'$(4\pi |\alpha|^2)^{-1/2}$',
                              r'$\left(16\pi^2 |\alpha|^2(1+|\alpha|^2)\right)^{-1/4}$']
                    #colors = plt.cm.inferno(np.linspace(0,1,4))
                    #colors = plt.cm.cividis(np.linspace(0,1,4))
                    #colors = plt.cm.magma(np.linspace(0,1,4))
                    #colors = plt.cm.plasma(np.linspace(0,1,4))
                    colors = plt.cm.viridis(np.linspace(0,1,4))
                    (line1,_,_) = plt.errorbar(x, mean[:,1], yerr=std[:,1],color=colors[0],label="1")
                    x_analytic = np.arange(1,10.5,0.5)
                    (line3,) = ax.plot(x_analytic, 1/np.sqrt(4*np.pi*x_analytic),color=colors[2],label="2")
                    (line4,) = ax.plot(x_analytic,
                        1/np.sqrt(4*np.pi*np.sqrt(x_analytic * np.sqrt(1+x_analytic**2))),
                         color=colors[3],label="3")
                    y_estimate = np.array((0.452203, 0.33867, 0.273943, 0.232666, 0.204477, 0.184209, 0.168998, 0.15715, 0.147623, 0.139751, 0.133099, 0.12737, 0.122361, 0.117925, 0.113956, 0.110374, 0.107117, 0.104138, 0.101397))
                    (line2,) = ax.plot(x_analytic, y_estimate, color=colors[1],label="4")
                    ax.set_ylim(0,0.5)
                    plt.legend((line1, line2, line3, line4), labels)
                elif plot == "DeltaQ_Noise_sq3" or plot == "DeltaQ_PL_vac":
                    ax.errorbar(x, mean[:,1], yerr=std[:,1], label=r'$\Delta_q$')
                    ax.errorbar(x, mean[:,0], yerr=std[:,0], label=r'$\Delta_p$')
                    ax.set_xlabel(r'$|\alpha|^2$')
                    plt.legend()
                fig.tight_layout()
                if plot is True:
                    fig.savefig(f"figures/squeezing_n_{str(q3)}{data_name_end}.{filetype}",bbox_inches='tight', pad_inches=0)
                elif plot == "DeltaQ_Noise_sq3":
                    fig.savefig(f"figures/{plot}_{str(q3)}.{filetype}",bbox_inches='tight', pad_inches=0)
                else:
                    fig.savefig(f"figures/{str(plot)}.{filetype}",bbox_inches='tight', pad_inches=0)
            if debug_figs:
                x = data["params"]["nrange"]
                dat_t = dat.transpose()

                # fig1, ax1 = plt.subplots()
                # ax1.boxplot(dat_t[1,:,:], positions=x, whis=[5, 95])
                # fig1.tight_layout()
                # fig1.savefig(f"figures/squeezing vs n whisker q - {str(q3)}.{filetype}")

                fig, ax = plt.subplots(1, 1, figsize=(3.4, 3.4))
                ax.boxplot(dat_t[1,:,:], positions=x, whis=[5, 95])
                ax.set_xlabel(r'$\bar{n}_A$')
                ax.set_ylabel(r'$\Delta_q$')
                fig.tight_layout()
                fig.savefig(f"figures/whisker_q_{str(q3)}{data_name_end}.{filetype}",bbox_inches='tight', pad_inches=0)

                fig, ax = plt.subplots(1, 1, figsize=(3.4, 3.4))
                ax.boxplot(dat_t[0,:,:], positions=x, whis=[5, 95])
                ax.set_xlabel(r'$\bar{n}_A$')
                ax.set_ylabel(r'$\Delta_p$')
                fig.tight_layout()
                fig.savefig(f"figures/whisker_p_{str(q3)}{data_name_end}.{filetype}",bbox_inches='tight', pad_inches=0)

                fig, ax = plt.subplots(1, 1, figsize=(3.4, 3.4))
                ax.boxplot(dat_t[2,:,:], positions=x, whis=[5, 95])
                ax.set_xlabel(r'$\bar{n}_A$')
                ax.set_ylabel(r'$\bar{n}_T$')
                fig.tight_layout()
                fig.savefig(f"figures/photons_{str(q3)}{data_name_end}.{filetype}",bbox_inches='tight', pad_inches=0)

                # fig, ax = plt.subplots()
                # ax.boxplot(np.abs(dat_t[3,:,:]), positions=x)
                # fig.tight_layout()
                # fig.savefig(f"figures/beta vs n - {str(q3)}.{filetype}")

                # fig, ax = plt.subplots()
                # ax.boxplot(np.angle(dat_t[3,:,:]), positions=x)
                # fig.tight_layout()
                # fig.savefig(f"figures/phi vs n - {str(q3)}.{filetype}")

    if to_sim == "square_plot":
        raise NotImplementedError

        """Settings """
        purity = False
        photons = False
        plot = True

        """System parameters """
        #params = {'kappa': [0.2, 2], 'g':1}          # Units in 100 kHz
        #params = {'kappa': [0.01, 0.4]}          # Units in 100 kHz
        params = {'kappa': None}          # Units in 100 kHz
        params['dims'] = (80, 20)
        params['ancilla_photons'] = 3
        params['measurement'] = 'max'
        #params['initial'] = ('squeezed', 3)
        params['initial'] = ('coherent', 0)
        #params['method'] = 'MC'
        params['method'] = 'Lindblad'

        """Initialize """
        if plot:
            fig1, ax1 = plt.subplots(3, 3, figsize=(12,12))
            fig2, ax2 = plt.subplots(3, 2, figsize=(12, 8))
            fig3, ax3 = plt.subplots(1, 4, figsize=(12, 4))
        system = System(params)

        """Simulate """
        print("Start", time.time()-START_TIME)
        if purity:
            print("Purity", system.purity())
        if photons:
            print("Photons",system.photons())
        print("Squeezing", system.effective_squeezing())
        if plot:
            system.plot_line(fig3, ax3[0], "Target", wmap=True, title="Target (Initial)")
            system.plot_line(fig3, ax3[1], "Ancilla", wmap=True, title="Ancilla (Initial)")
        #system.plot(ax1[0], husimi=False, wmap=False)
        #system.plot_fock(fig2, ax2[0])
        print("Evolution Q", time.time()-START_TIME)
        system.update('q')
        if photons:
            print("Photons",system.photons(), "pre disp")
        system.displace(1j*np.sqrt(2)*params['ancilla_photons'])
        if photons:
            print("Photons",system.photons(), "post disp")
        print("Plot. Time:", time.time()-START_TIME)
        system.plot_line(fig3, ax3[2], "Husimi", wmap=True)
        system.plot_line(fig3, ax3[3], "Target", wmap=True, title="Target (Final)")
        #system.plot(ax1[1], wmap=False)
        #system.plot_fock(fig2, ax2[1])
        print("Evolution P", time.time()-START_TIME)
        #system.update('p')
        #system.displace(-2)
        print("Plot. Time:", time.time()-START_TIME)
        #system.plot(ax1[2], wmap=False)
        #system.plot_fock(fig2, ax2[2])
        print("Done", time.time()-START_TIME)
        print(system.purity())
        print(system.effective_squeezing())

        #fig1.tight_layout()
        #fig1.savefig('wigner_me_vac.pdf')
        #fig2.tight_layout()
        #fig2.savefig('fock_vac.pdf')
        fig3.tight_layout()
        fig3.savefig('wigner_line_vac_3_order3.pdf')

    yappi.get_func_stats().print_all()
    yappi.get_thread_stats().print_all()