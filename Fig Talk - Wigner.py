import time

import matplotlib
import matplotlib.pyplot as plt
import numpy as np
import qutip

from functions import System, low_priority, Params

low_priority()
if __name__ == "__main__":
    font_size = 30
    matplotlib.rcParams['mathtext.fontset'] = 'cm'
    matplotlib.rc('text', usetex=True)
    matplotlib.rcParams.update({'font.size': font_size})
    matplotlib.rcParams['mathtext.fontset'] = 'cm'
    matplotlib.rcParams['axes.labelsize'] = font_size
    matplotlib.rc('text', usetex=True)
    matplotlib.rcParams['xtick.labelsize'] = font_size
    matplotlib.rcParams['ytick.labelsize'] = font_size
    matplotlib.rcParams['legend.fontsize'] = font_size

    START_TIME = time.time()

    #import os
    #data_dir = 'data/'
    #data_dir = 'python/data/'
    #assert os.path.exists(data_dir)

    # """Settings """
    purity = False

    """System parameters """
    #params = Params(0.1 * np.sqrt(2 * np.pi))

    #for plot in ("sq3", "vac", 'vac_sensor'):
    for plot in ('vac_sensor',):
        if plot == "vac":
            params = Params("GKP")
            params.initial_T = ('coherent', 0)
        elif plot == "sq3":
            params = Params("GKP")
            params.initial_T = ('squeezed', 3)
        elif plot == "eff":
            params = Params("GKP")
            params.initial_T = ('coherent', 0)
            params.thermal_meas = 2
        elif plot == 'vac_sensor':
            params = Params("sensor")
            params.initial_T = ('coherent', 0)
        else:
            raise NotImplementedError

        params.dims = (150, 15)
        params.tidyup = True

        params.measurement = 'fixed'
        params.initial_T = ('coherent', 0)
        params.method = 'Lindblad'
        params.ancilla_photons = 3
        params.w_range = 6
        params.n_points = 201

        # """Initialize """
        #fig3, ax3 = plt.subplots(1, 1, figsize=(4, 4))
        fig3, ax3 = plt.subplots(1, 1)
        system = System(params)

        # """Simulate """
        system.plot_line(fig3, ax3, "Target", wmap=True, title="")
        fig3.tight_layout(pad=0.3, w_pad=0, h_pad=0)
        fig3.savefig(f'figures/talk_gkp_{str(plot)}.pdf', bbox_inches='tight', pad_inches=0)

        #fig3, ax3 = plt.subplots(1, 1, figsize=(4, 4))
        fig3, ax3 = plt.subplots(1, 1)
        print("Evolution Q", time.time() - START_TIME)
        system.update('q')
        print("Plot Target 1, Time:", time.time() - START_TIME)
        system.plot_line(fig3, ax3, "Target", wmap=True, title="")
        fig3.tight_layout(pad=0.3, w_pad=0, h_pad=0)
        fig3.savefig(f'figures/talk_gkp_{str(plot)}_post.pdf', bbox_inches='tight', pad_inches=0)

        #fig3, ax3 = plt.subplots(1, 1, figsize=(4, 4))
        fig3, ax3 = plt.subplots(1, 1)
        print("Plot Husimi, Time:", time.time() - START_TIME)
        system.plot_line(fig3, ax3, "Husimi", title="")
        fig3.tight_layout(pad=0.3, w_pad=0, h_pad=0)
        fig3.savefig(f'figures/talk_gkp_{str(plot)}_husimi.pdf', bbox_inches='tight', pad_inches=0)
    
      #for plot in ("sq3", "vac", 'vac_sensor'):
    params = Params("GKP")
    N = params.dims[0]
    cat = (qutip.displace(N, -np.sqrt(3))+qutip.displace(N, np.sqrt(3))) * qutip.basis(N,0)
    params.initial_T = ('custom', cat.unit())
    params.ancilla_photons = 3
    # """Initialize """
    #fig3, ax3 = plt.subplots(1, 1, figsize=(4, 4))
    fig3, ax3 = plt.subplots(1, 1)
    system = System(params)

    # """Simulate """
    system.plot_line(fig3, ax3, "Target", wmap=True, title="")
    fig3.tight_layout(pad=0.3, w_pad=0, h_pad=0)
    fig3.savefig(f'figures/talk_gkp_cat.pdf', bbox_inches='tight', pad_inches=0)