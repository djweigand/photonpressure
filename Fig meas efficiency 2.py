import pickle
import time
import warnings


import matplotlib
import matplotlib.pyplot as plt
import numpy as np

from functions import (Params, Samples, low_priority, parallel_map)

if __name__ == "__main__":
    low_priority()

    matplotlib.rcParams['mathtext.fontset'] = 'cm'
    matplotlib.rc('text', usetex=True)


    START_TIME = time.time()

    warnings.warn("There is a bug in MKL. Use the workarounds from module functions or other BLAS versions of numpy/scipy. Check version using np.__config__.show()")
    import os
    #data_dir = '../data/'
    data_dir = 'python/data/'
    assert os.path.exists(data_dir)

    print(os.getcwd())

    plot=True

    """Settings """
    purity = False

    plot = "Kerr"
    debug_figs = False
    filetype = "pdf"
    print_p = True
    #figname = 'gkp_meas_eff_pq3'

    load_data = False

    save_data = True
    save_new = False

    """System parameters """
    #params = Params(0.1 * np.sqrt(2 * np.pi))
    params = Params("GKP")
    params.nr_samples = 200
    params.n_range = np.arange(1, 3.5, 0.5)
    #params.n_range = np.arange(2.5, 3.5, 0.5)
    #params.dims = (200, 20)
    params.dims = (300, 200)
    params.tidyup = True

    params.measurement = 'random'
    params.initial_T = ('coherent', 0)
    params.method = 'Lindblad'

    thermal_photons = np.array([0, 1, 5, 10, 50])
    #thermal_photons = np.array([1, 5, 10, 25, 50])

    if save_data is True:
        print(str(thermal_photons))
        #assert input("OVERWRITING saved data. Type \"OK\" to continue.") == "OK"
    elif load_data == False and save_data == False:
        pass
        #assert input("Generating new data WITHOUT SAVING. Type \"OK\" to continue.") == "OK"


    for n_th in thermal_photons:
        print("rate=", n_th, "time=", time.time()-START_TIME)

        data_name = f"vac_gkp_measeff_{n_th}_3"

        dq = []
        dp = []
        dq_err = []
        dp_err = []
        full_q = []
        full_p = []

        if save_new:
            if os.path.isfile(data_dir+data_name):
                load_data = True
            else:
                load_data = False

        if load_data:
            assert  os.path.isfile(data_dir+data_name)
        else:
            data = {"params": params}
            dat = []
            for n in params.n_range:
                params.ancilla_photons = n
                params.thermal_meas = n_th

                # Initialize
                print("n=", n, "time=", time.time()-START_TIME)
                system = Samples(params)
                #system.tidyup()

                # Simulate
                if params.measurement == 'fixed':
                    raise Exception
                    # system.update('q')
                    # print("meas done, time=", time.time()-START_TIME)
                    # q, p = system.result
                    # print("photon nr, time=", time.time()-START_TIME)
                    # na = system.photons()[0]
                    # print("Squeezing, time=", time.time()-START_TIME)
                    # delta_p, delta_q = system.effective_squeezing() # pylint: disable=unbalanced-tuple-unpacking
                    # out = [[delta_p, delta_q, na, (q+ 1j*p)/np.sqrt(2)],[delta_p, delta_q, na, (q+ 1j*p)/np.sqrt(2)]]
                else:
                    system.pre_measurement('q')
                    #system.tidyup()
                    print("pre meas done, time=", time.time()-START_TIME)
                    #out = parallel_map(system.sample, np.arange(params.nr_samples), chunksize=int(params.nr_samples/16), max_workers=5)
                    out = parallel_map(system.sample, np.arange(params.nr_samples))
                    #out = [system.sample(idx) for idx in np.arange(params.nr_samples)]
                dat.append(out)
            data["data"] = np.real_if_close(np.array(dat))

            if save_data:
                with open(data_dir+data_name, "wb") as file:
                    pickle.dump(data, file, protocol=-1)

    lines = []
    labels = []
    #fig, ax = plt.subplots(1, 1, figsize=(3.4, 3.4))
    fig, ax = plt.subplots(1, 1, figsize=(5, 5))
    colors = plt.cm.viridis(np.linspace(0, 1, len(thermal_photons)))    # pylint: disable=no-member
    for i, n_th in enumerate(thermal_photons):
        #figname = 'None_vac'
        figname = 'gkp_meas_eff_pq3'
        data_name = f"vac_gkp_measeff_{n_th}_3"
        #data_name = 'None_vac'
        #data_name = f"vac_gkp_measeff_{n_th}"
        with open(data_dir+data_name, "rb") as file:
            data = pickle.load(file)

        print(" ")

        dat = data["data"]
        mean = np.mean(dat, axis=1)
        std = np.std(dat, axis=1)
        print(mean[:,1])
        x = data["params"].n_range

        (line,_,_) = ax.errorbar(x, np.real_if_close(mean[:,1]), yerr=np.real_if_close(std[:,1]), label=str(n_th), color=colors[i], capsize=6, linestyle='-')
        if print_p:
            ax.errorbar(x, np.real_if_close(mean[:,0]), yerr=np.real_if_close(std[:,0]), label=str(n_th), color=colors[i], capsize=6, linestyle='--')

        lines.append(line)
        labels.append(str(n_th))

    x_analytic = np.arange(1, 10.5, 0.5)
    ones = np.ones_like(x_analytic)
    #ax.plot(x_analytic, ones)
    ax.set_ylim(0, 2)
    plt.legend(lines, labels)
    fig.tight_layout()
    fig.savefig(f"figures/{figname}.{filetype}",bbox_inches='tight', pad_inches=0)
