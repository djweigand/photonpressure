#%%
import concurrent.futures
import os
from functools import partial

import matplotlib
import matplotlib.animation as animation
import matplotlib.pyplot as plt
import numpy as np
import psutil
import qutip

from functions import parallel_map

process = psutil.Process(os.getpid())
process.nice(psutil.IDLE_PRIORITY_CLASS)
process.ionice(psutil.IOPRIO_VERYLOW)

if __name__ == "__main__":
    font_size = 30

    os.chdir("talk/figures/")
    matplotlib.rcParams['mathtext.fontset'] = 'cm'
    matplotlib.rc('text', usetex=True)
    matplotlib.rcParams.update({'font.size': font_size})
    matplotlib.rcParams['mathtext.fontset'] = 'cm'
    matplotlib.rcParams['axes.labelsize'] = font_size
    matplotlib.rc('text', usetex=True)
    matplotlib.rcParams['xtick.labelsize'] = font_size
    matplotlib.rcParams['ytick.labelsize'] = font_size
    matplotlib.rcParams['legend.fontsize'] = font_size
    N = 100

    delta = 0.3
    r = np.log(1 / delta)
    #%%
    sq_vac = qutip.squeeze(N, r) * qutip.basis(N, 0)

    k = np.arange(-3,3+1)
    w_range0 = 6
    xvec = np.linspace(-w_range0, w_range0, 200)

    wig = partial(qutip.wigner, xvec=xvec, yvec=xvec)
    qf = partial(qutip.qfunc, xvec=xvec, yvec=xvec)

    #++++++++++++++++++++++++++++++
    # Plot disp. squeezed vac (\pi) Target
    w_range = w_range0/np.sqrt(np.pi)
    disp_sq_vac = (qutip.displace(N, np.sqrt(2*np.pi)/4) * sq_vac).unit()

    w = wig(disp_sq_vac)

    fig, ax = plt.subplots(1,1)
    m = np.max(w)
    norm = matplotlib.colors.Normalize(-m, m)
    cmap = plt.get_cmap('seismic_r')
    ax.axhline(linewidth=0.1, color='k')
    ax.axvline(linewidth=0.1, color='k')
    ax.set_xlabel(r'$q/\sqrt{\pi}$')
    ax.set_ylabel(r'$p/\sqrt{\pi}$')
    ax.set_xticks(np.arange(-3, 4))
    ax.set_yticks(np.arange(-3, 4))

    ax.imshow(w, origin='lower', animated=True, norm=norm, cmap=cmap, extent=(-w_range, w_range, -w_range, w_range))

    fig.tight_layout(pad=0.3, w_pad=0, h_pad=0)
    fig.savefig(f'disp_sq_pi.svg', bbox_inches='tight', pad_inches=0)

    #++++++++++++++++++++++++++++++
    # Plot disp. squeezed vac (\pi) Ancilla
    w_range = w_range0/np.sqrt(np.pi)
    coh = (qutip.coherent(N, -np.sqrt(3))).unit()

    w = wig(coh)

    fig, ax = plt.subplots(1,1)
    m = np.max(w)
    norm = matplotlib.colors.Normalize(-m, m)
    cmap = plt.get_cmap('seismic_r')
    ax.axhline(linewidth=0.1, color='k')
    ax.axvline(linewidth=0.1, color='k')
    ax.set_xlabel(r'$q/\sqrt{\pi}$')
    ax.set_ylabel(r'$p/\sqrt{\pi}$')
    ax.set_xticks(np.arange(-3, 4))
    ax.set_yticks(np.arange(-3, 4))

    ax.imshow(w, origin='lower', animated=True, norm=norm, cmap=cmap, extent=(-w_range, w_range, -w_range, w_range))

    fig.tight_layout(pad=0.3, w_pad=0, h_pad=0)
    fig.savefig(f'disp_sq_pi_ancilla.svg', bbox_inches='tight', pad_inches=0)

    #++++++++++++++++++++++++++++++
    # Plot \alpha=\sqrt{3} Ancilla
    w_range = w_range0/np.sqrt(2*np.pi)
    coh = (qutip.coherent(N, np.sqrt(3))).unit()

    w = wig(coh)

    fig, ax = plt.subplots(1,1)
    m = np.max(w)
    norm = matplotlib.colors.Normalize(-m, m)
    cmap = plt.get_cmap('seismic_r')
    ax.axhline(linewidth=0.1, color='k')
    ax.axvline(linewidth=0.1, color='k')
    ax.set_xlabel(r'$q/\sqrt{\pi}$')
    ax.set_ylabel(r'$p/\sqrt{\pi}$')
    ax.set_xticks(np.arange(-2, 3))
    ax.set_yticks(np.arange(-2, 3))

    ax.imshow(w, origin='lower', animated=True, norm=norm, cmap=cmap, extent=(-w_range, w_range, -w_range, w_range))

    fig.tight_layout(pad=0.3, w_pad=0, h_pad=0)
    fig.savefig(f'coherent3_sensor.svg', bbox_inches='tight', pad_inches=0)

    exit()

    #++++++++++++++++++++++++++++++
    # Plot disp. squeezed vac (\pi/2) Target
    w_range = w_range0/np.sqrt(np.pi)
    disp_sq_vac = (qutip.displace(N, np.sqrt(2*np.pi)/8) * sq_vac).unit()

    w = wig(disp_sq_vac)

    fig, ax = plt.subplots(1,1)
    m = np.max(w)
    norm = matplotlib.colors.Normalize(-m, m)
    cmap = plt.get_cmap('seismic_r')
    ax.axhline(linewidth=0.1, color='k')
    ax.axvline(linewidth=0.1, color='k')
    ax.set_xlabel(r'$q/\sqrt{\pi}$')
    ax.set_ylabel(r'$p/\sqrt{\pi}$')
    ax.set_xticks(np.arange(-3, 4))
    ax.set_yticks(np.arange(-3, 4))

    ax.imshow(w, origin='lower', animated=True, norm=norm, cmap=cmap, extent=(-w_range, w_range, -w_range, w_range))

    fig.tight_layout(pad=0.3, w_pad=0, h_pad=0)
    fig.savefig(f'disp_sq_05pi.svg', bbox_inches='tight', pad_inches=0)

    #++++++++++++++++++++++++++++++
    # Plot disp. squeezed vac (\pi/2) Ancilla
    w_range = w_range0/np.sqrt(np.pi)
    disp_sq_vac = (qutip.displace(N, np.sqrt(2*np.pi)/8) * sq_vac).unit()

    w = wig(disp_sq_vac)

    fig, ax = plt.subplots(1,1)
    m = np.max(w)
    norm = matplotlib.colors.Normalize(-m, m)
    cmap = plt.get_cmap('seismic_r')
    ax.axhline(linewidth=0.1, color='k')
    ax.axvline(linewidth=0.1, color='k')
    ax.set_xlabel(r'$q/\sqrt{\pi}$')
    ax.set_ylabel(r'$p/\sqrt{\pi}$')
    ax.set_xticks(np.arange(-3, 4))
    ax.set_yticks(np.arange(-3, 4))

    ax.imshow(w, origin='lower', animated=True, norm=norm, cmap=cmap, extent=(-w_range, w_range, -w_range, w_range))

    fig.tight_layout(pad=0.3, w_pad=0, h_pad=0)
    fig.savefig(f'disp_sq_05pi_ancilla.svg', bbox_inches='tight', pad_inches=0)



    #++++++++++++++++++++++++++++++
    # Plot GKP 0
    w_range = w_range0/np.sqrt(np.pi)
    gkp0 = qutip.Qobj(dims=[[N],[1]])
    for k_ in k:
        gkp0 += (np.exp(-2 * np.pi*k_**2 * delta**2)
                    * qutip.displace(N, k_*np.sqrt(2*np.pi)) * sq_vac)
    gkp0 = gkp0.unit()

    w = wig(gkp0)

    fig, ax = plt.subplots(1,1)
    m = np.max(w)
    norm = matplotlib.colors.Normalize(-m, m)
    cmap = plt.get_cmap('seismic_r')
    ax.axhline(linewidth=0.1, color='k')
    ax.axvline(linewidth=0.1, color='k')
    ax.set_xlabel(r'$q/\sqrt{\pi}$')
    ax.set_ylabel(r'$p/\sqrt{\pi}$')
    ax.set_xticks(np.arange(-3, 4))
    ax.set_yticks(np.arange(-3, 4))

    ax.imshow(w, origin='lower', animated=True, norm=norm, cmap=cmap, extent=(-w_range, w_range, -w_range, w_range))

    fig.tight_layout(pad=0.3, w_pad=0, h_pad=0)
    fig.savefig(f'gkp0.svg', bbox_inches='tight', pad_inches=0)

    #++++++++++++++++++++++++++++++
    # Plot Sensor state Wigner func
    w_range = w_range0/np.sqrt(2*np.pi)
    sensor = qutip.Qobj(dims=[[N],[1]])
    for k_ in k:
        sensor += (np.exp(-np.pi*k_**2 * delta**2)
                    * qutip.displace(N, k_*np.sqrt(np.pi)) * sq_vac)
    sensor = sensor.unit()

    w = wig(sensor)

    fig, ax = plt.subplots(1,1)
    m = np.max(w)
    norm = matplotlib.colors.Normalize(-m, m)
    cmap = plt.get_cmap('seismic_r')
    ax.axhline(linewidth=0.1, color='k')
    ax.axvline(linewidth=0.1, color='k')
    ax.set_xlabel(r'$q/\sqrt{2\pi}$')
    ax.set_ylabel(r'$p/\sqrt{2\pi}$')
    ax.set_xticks(np.arange(-2, 3))
    ax.set_yticks(np.arange(-2, 3))

    ax.imshow(w, origin='lower', animated=True, norm=norm, cmap=cmap, extent=(-w_range, w_range, -w_range, w_range))

    fig.tight_layout(pad=0.3, w_pad=0, h_pad=0)
    fig.savefig(f'sensor.svg', bbox_inches='tight', pad_inches=0)


    #++++++++++++++++++++++++++++++
    # Animate sensor state wigner under photon loss
    # The reverse is a good visual to explain
    # how the uncertainty can be circumvented
    w_range = w_range0/np.sqrt(2*np.pi)
    a = qutip.destroy(N)
    c_ops = [a]
    tlist = np.linspace(0,3, 50)
    states = qutip.mesolve(qutip.Qobj(dims=[[N],[N]]), sensor, tlist, c_ops, options=qutip.Options(nsteps=100000)).states

    with concurrent.futures.ProcessPoolExecutor() as executor:
        w = parallel_map(wig, states, executor)

    fig, ax = plt.subplots(1,1)
    m = np.max(w)
    norm = matplotlib.colors.Normalize(-m, m)
    cmap = plt.get_cmap('seismic_r')
    ax.axhline(linewidth=0.1, color='k')
    ax.axvline(linewidth=0.1, color='k')
    ax.set_xlabel(r'$q/\sqrt{2\pi}$')
    ax.set_ylabel(r'$p/\sqrt{2\pi}$')
    ax.set_xticks(np.arange(-2, 3))
    ax.set_yticks(np.arange(-2, 3))


    ims = []
    m = np.max(w)
    norm = matplotlib.colors.Normalize(-m, m)
    cmap = plt.get_cmap('seismic_r')
    for d in w:
        im = ax.imshow(d, origin='lower', norm=norm, cmap=cmap, extent=(-w_range, w_range, -w_range, w_range))
        ims.append([im])
    ani = animation.ArtistAnimation(fig, ims, interval=100, blit=True,
                            repeat_delay=1000, repeat=False)

    savefig_kwargs = {"pad_inches":0}
    fig.tight_layout(pad=0.3, w_pad=0, h_pad=0)
    ani.save(f'handwavy.mp4',savefig_kwargs=savefig_kwargs)
    os.system(f"ffmpeg -y -loglevel warning -i handwavy.mp4 -loop 0 -final_delay 1500 -vf reverse handwavy.gif")

    #++++++++++++++++++++++++++++++
    # Animate sensor state qfunc under photon loss
    # The reverse is a good visual to explain
    # how the uncertainty can be circumvented
    w_range = w_range0/np.sqrt(2*np.pi)
    with concurrent.futures.ProcessPoolExecutor() as executor:
        w = parallel_map(qf, states, executor)

    fig, ax = plt.subplots(1,1)
    m = np.max(w)
    norm = matplotlib.colors.Normalize(-m, m)
    cmap = plt.get_cmap('seismic_r')
    ax.axhline(linewidth=0.1, color='k')
    ax.axvline(linewidth=0.1, color='k')
    ax.set_xlabel(r'$q/\sqrt{2\pi}$')
    ax.set_ylabel(r'$p/\sqrt{2\pi}$')
    ax.set_xticks(np.arange(-2, 3))
    ax.set_yticks(np.arange(-2, 3))


    ims = []
    m = np.max(w)
    norm = matplotlib.colors.Normalize(-m, m)
    cmap = plt.get_cmap('seismic_r')
    for d in w:
        im = ax.imshow(d, origin='lower', norm=norm, cmap=cmap, extent=(-w_range, w_range, -w_range, w_range))
        ims.append([im])
    ani = animation.ArtistAnimation(fig, ims, interval=100, blit=True,
                            repeat_delay=1000, repeat=False)

    savefig_kwargs = {"pad_inches":0}
    fig.tight_layout(pad=0.3, w_pad=0, h_pad=0)
    ani.save(f'handwavy_q.mp4',savefig_kwargs=savefig_kwargs)
    os.system(f"ffmpeg -y -loglevel warning -i handwavy_q.mp4 -loop 0 -final_delay 1500 -vf reverse handwavy_q.gif")
