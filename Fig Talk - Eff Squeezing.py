import time

import matplotlib
import matplotlib.pyplot as plt
import numpy as np
import pickle
import concurrent.futures
import seaborn as sns

from functions import (Params, Samples, low_priority, parallel_map)


if __name__ == "__main__":
    low_priority()
    font_size = 20
    matplotlib.rcParams['mathtext.fontset'] = 'cm'
    matplotlib.rc('text', usetex=True)
    matplotlib.rcParams.update({'font.size': font_size})
    matplotlib.rcParams['mathtext.fontset'] = 'cm'
    matplotlib.rcParams['axes.labelsize'] = font_size
    matplotlib.rc('text', usetex=True)
    matplotlib.rcParams['xtick.labelsize'] = font_size
    matplotlib.rcParams['ytick.labelsize'] = font_size
    matplotlib.rcParams['legend.fontsize'] = font_size

    START_TIME = time.time()

    import os
    #data_dir = 'data/'
    data_dir = 'python/data/'
    assert os.path.exists(data_dir)

    plot=True

    # """Settings """
    purity = False
    photons = False

    plot = "DeltaQ_estimate_vac"
    debug_figs = True
    filetype = "pdf"

    load_data = True
    save_data = False

    params = Params("GKP")
    params.nr_samples = 200
    params.n_range = np.arange(1,4.5,0.5)
    params.dims = (500, 20)
    params.tidyup = True

    params.measurement = 'random'
    params.initial_T = ('coherent', 0)
    params.method = 'Lindblad'

    """System parameters """
    assert plot == "DeltaQ_estimate_vac"
    data_name_end = "_vac"
    noise_types = (None,)

    if save_data is True:
        print(str(noise_types)+data_name_end)
        ok = input("Overwriting saved data. Type \"OK\" to continue.")
        assert ok == "OK"
    elif load_data == False and save_data == False:
        ok = input("Generating new data without saving. Type \"OK\" to continue.")
        assert ok == "OK"

    for q3 in noise_types:
        params.q3 = q3

        dq = []
        dp = []
        dq_err = []
        dp_err = []
        full_q = []
        full_p = []

        if load_data:
            with open(data_dir+str(q3)+data_name_end, "rb") as file:
                data = pickle.load(file)
        else:
            data = {"params": params}
            dat = []
            for n in params.n_range:
                params.ancilla_photons = n
                """Initialize """
                system = Samples(params)
                print("Squeezing", system.effective_squeezing())

                """Simulate """
                print("n=", n, "time=", time.time()-START_TIME)
                system.pre_measurement('q')
                print("pre meas done, time=", time.time()-START_TIME)
                with concurrent.futures.ProcessPoolExecutor(max_workers=6) as executor:
                    out = list(executor.map(system.sample, range(params.nr_samples)))
                dat.append(out)
            data["data"] = np.array(dat)

            if save_data:
                with open(data_dir+str(q3)+data_name_end, "wb") as file:
                    pickle.dump(data, file, protocol=-1)

        if plot:
            dat = data["data"]
            x = data["params"]["nrange"]

            mean = np.mean(dat, axis=1)
            std = np.std(dat, axis=1)

            fig, ax = plt.subplots(1, 1, figsize=(5,5))
            if plot == "DeltaQ_estimate_vac":
#                labels = [r'$\langle\Delta_q\rangle$',
#                            r"Villain",
#                            r'$(4\pi |\alpha|^2)^{-1/2}$',
#                            r'$\left(16\pi^2 |\alpha|^2(1+|\alpha|^2)\right)^{-1/4}$']
                labels = ['numerics', 'analyt. bounds']
                c = plt.cm.viridis(np.linspace(0,1,4)) # pylint disable=no-member
                colors = [c[2], 'grey', 'grey', 'grey']
                x_analytic = np.arange(1,params.n_range[-1]+0.5,0.5)
                (line3,) = ax.plot(x_analytic, 1/np.sqrt(4*np.pi*x_analytic),color=colors[2],label="2")
                (line4,) = ax.plot(x_analytic,
                    1/np.sqrt(4*np.pi*np.sqrt(x_analytic * np.sqrt(1+x_analytic**2))),
                        color=colors[3],label="3")
                y_estimate_ = np.array((0.452203, 0.33867, 0.273943, 0.232666, 0.204477, 0.184209, 0.168998, 0.15715, 0.147623, 0.139751, 0.133099, 0.12737, 0.122361, 0.117925, 0.113956, 0.110374, 0.107117, 0.104138, 0.101397))
                y_estimate = np.array([y_estimate_[i] for i in range(len(x_analytic))])
                (line2,) = ax.plot(x_analytic, y_estimate, color=colors[2],label="4")
                ax.set_ylim(0,0.5)
                #(line1,_,_) = plt.errorbar(x, mean[:,1], yerr=std[:,1],color=colors[0],label="1",zorder=10)
                sns.violinplot(data=np.transpose(dat[:,:,1]))
                ax.set_xlabel(r'$\bar{n}_A$')
                ax.set_ylabel(r'$\langle \Delta_q\rangle$')
                ax.set_xticks([1,4])
                ax.set_yticks([0,0.25,0.5])
                #plt.legend((line1, line2), labels)
            elif plot == "DeltaQ_Noise_sq3" or plot == "DeltaQ_PL_vac":
                ax.errorbar(x, mean[:,1], yerr=std[:,1], label=r'$\Delta_q$')
                ax.errorbar(x, mean[:,0], yerr=std[:,0], label=r'$\Delta_p$')
                ax.set_xlabel(r'$|\alpha|^2$')
                plt.legend()
            fig.tight_layout()
            if plot is True:
                fig.savefig(f"figures/squeezing_n_{str(q3)}{data_name_end}_talk_violin.{filetype}",bbox_inches='tight', pad_inches=0)
            elif plot == "DeltaQ_Noise_sq3":
                fig.savefig(f"figures/{plot}_{str(q3)}_talk_violin.{filetype}",bbox_inches='tight', pad_inches=0)
            else:
                fig.savefig(f"figures/{str(plot)}_talk_violin.{filetype}",bbox_inches='tight', pad_inches=0)
        if debug_figs:
            x = data["params"]["nrange"]
            dat_t = dat.transpose()
