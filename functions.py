import concurrent.futures
import copy
import os
from concurrent.futures import as_completed
from functools import partial
from typing import Any, Callable, Iterable, List, Optional, Tuple, Union

import matplotlib
import matplotlib.animation as animation
from matplotlib.animation import writers, _log
import matplotlib.pyplot as plt
import numpy as np
import psutil
import qutip
import scipy.linalg as la
import scipy.signal
from matplotlib import cbook, rc_context, rcParams, rcParamsDefault
from mpl_toolkits.axes_grid1 import make_axes_locatable
from qutip import (basis, coherent, destroy, displace, expect, liouvillian,
                   mcsolve, mesolve, operator_to_vector,
                   plot_fock_distribution, qeye, squeeze, tensor, thermal_dm,
                   vector_to_operator, wigner, wigner_cmap)
from qutip.qobj import Qobj, isket, isoper
from scipy import (
    arange, conjugate, exp, fliplr, meshgrid, pi, polyval, real, size, sqrt,
    zeros)
from scipy.special import factorial
from tqdm import tqdm


class Params:
    def __init__(self, state_type):
        if state_type == 'GKP':
            self.spacing = 2 * np.sqrt(np.pi)
        elif state_type == 'sensor':
            self.spacing = np.sqrt(2 * np.pi)
        elif isinstance(state_type, float):
            self.spacing = state_type
        else:
            raise NotImplementedError

        self.dims = (200, 20)
        self.w_range = 13
        self.n_points = 401
        self.ntraj = None
        self.t_steps = None
        self.measurement = 'random'
        self.method = 'Lindblad'
        self.tidyup = False

        # System setup
        self.kappa_T = None
        self.kappa_A = None
        self.g0 = 1
        self.q3 = None
        self.q3_strength = None
        self.initial_T = None
        self.initial_A = None
        self.ancilla_photons = None
        self.thermal_meas = None

        self.thermal_envelope = None

        self.t_out = None
        self.husimi = None
        self.unconditional = True

        # Not used, but stored for archive
        self.nr_samples = None    # How many samples are taken from the final state
        self.n_range = None       # Range of initial photon number of ancilla

        self.time_dependent = False
        self.kerr_type = None
        self.kerr_strength = None
        self.drive_freq = None

        # only for initialization. Should not be set manually
        self.c_ops = []
        self.Hamiltonian = {}
        self.mesolve_args = None
        self.state_list = None
        self._state = None

    def sanity_test(self):
        assert (self.kappa_T is None) == (self.kappa_A is None)
        assert (self.q3 is None) == (self.q3_strength is None)
        assert self.method in ('Lindblad', 'Lindblad2', 'MC', 'MC2'), 'Method not defined'
                # Method 'MC2' is used for the initial state for sampling
                # Method 'Lindblad2' is used if the initial state is thermal
        assert self.method != 'Liouville', 'Liouville needs dim^4 as memory, this is too much (can be TB)'
        if self.kerr_type is None:
            assert self.kerr_strength is None
            assert self.time_dependent == False, self.time_dependent
            assert self.drive_freq is None, self.drive_freq
        else:
            assert self.time_dependent is not None
            assert self.drive_freq is not None
            assert (type(self.kerr_strength) == list) or (type(self.kerr_strength) == float)


    def initial_state(self, idx)->Union[qutip.Qobj, Tuple[qutip.Qobj, np.array]]:
        initial = (self.initial_T, self.initial_A)[idx]

        if idx == 1:
            if initial[0] == 'disp_thermal':
                assert len(initial) == 3
                assert self.method == 'Lindblad2'
                disp = displace(self.dims[idx], np.sqrt(initial[1]))
                n = np.arange(self.dims[idx])
                n_th = initial[2]
                state_list = [disp * basis(self.dims[idx], n_) for n_ in n]
                thermal_envelope = (1+1/n_th)**(-n)/(1+n_th)
                return state_list, thermal_envelope

        if initial[0] == 'thermal':
            assert idx == 1
            assert self.method != 'MC2'
            state = thermal_dm(self.dims[idx], initial[1])
        elif initial[0] == 'squeezed':
            r = np.log(1 / initial[1])
            state = squeeze(self.dims[idx], r) * basis(self.dims[idx], 0)
        elif initial[0] == 'coherent':
            state = coherent(self.dims[idx], initial[1])
        elif initial[0] == 'custom':
            assert type(initial[1]) == qutip.Qobj
            state = initial[1]
        else:
            raise NotImplementedError(f"Invalid input: {initial} Valid arguments: ('thermal', photons); ('squeezed', delta); ('coherent', alpha), ('custom', qutip.Qobj)")
        return state

class System(Params):
    def __init__(self, params)->None:
        assert isinstance(params, Params)
        params.sanity_test()
        self.__dict__ = copy.deepcopy(params.__dict__)

        if self.initial_A is None:
            assert self.ancilla_photons is not None
            self.initial_A = ('coherent', np.sqrt(self.ancilla_photons))
        elif self.ancilla_photons is None:
            if self.initial_A[0] == 'coherent':
                self.ancilla_photons = np.abs(self.initial_A[1])**2
            elif self.initial_A[0] == 'disp_thermal':
                self.ancilla_photons = np.abs(self.initial_A[1])**2 + self.initial_A[2]
            else: raise Exception
        else: raise Exception

        if self.initial_A[0] == "disp_thermal":
            state_0 = self.initial_state(0)
            state_1, self.thermal_envelope = self.initial_state(1)
            assert isket(state_0)
            self.state_list = [tensor(state_0, state) for state in state_1]
        else:
            self._state = tensor(self.initial_state(0), self.initial_state(1))


        if self.t_out is None:
            self.t_out = self.spacing / self.g0
        else:
            self.t_out = self.t_out * self.spacing / self.g0
        # Derived Settings
        self.qvec = np.linspace(-self.w_range, self.w_range, self.n_points)
        self.pvec = np.linspace(-self.w_range, self.w_range, self.n_points)

        if self.t_steps is None:
            self.t = np.linspace(0, self.t_out, 2)
        else:
            self.t = np.linspace(0, self.t_out, self.t_steps)
        self.stabilizers = [displace(self.dims[0], self.spacing/np.sqrt(2)), displace(self.dims[0], 1j*self.spacing/np.sqrt(2))]
        self.stabilizers_full = [tensor(stab, qeye(self.dims[1])) for stab in self.stabilizers]

        if self.method == 'Liouville':
            self.time_evo = {}
            for quadrature in ['q', 'p']:
                L = self.t_out * liouvillian(self.Hamiltonian['q'], c_ops=self.c_ops)
                self.time_evo[quadrature] = L.expm(method='sparse')

        a = tensor(destroy(self.dims[0]), qeye(self.dims[1]))
        b = tensor(qeye(self.dims[0]), destroy(self.dims[1]))
        self.na = a.dag() * a
        self.nb = b.dag() * b
        self.na_red = destroy(self.dims[0]).dag() * destroy(self.dims[0])

        if self.kappa_T:
            self.c_ops.append(np.sqrt(self.kappa_T) * a)
        if self.kappa_A:
            self.c_ops.append(np.sqrt(self.kappa_A) * b)
        if self.Hamiltonian == {}:
            self.Hamiltonian['q'] = self.g0 * (a.dag() + a) * b.dag() * b / np.sqrt(2)
            self.Hamiltonian['p'] = 0.5*1j* self.g0 * (a.dag() - a) * b.dag() * b / np.sqrt(2)
            # This is an unconditional drive in order to keep the photon number down
            if self.unconditional:
                self.Hamiltonian['q'] -= self.g0 * (a.dag() + a) * self.ancilla_photons / (2*np.sqrt(2))
                self.Hamiltonian['p'] -= 0.5*1j* self.g0 * (a.dag() - a) * b.dag() * b / (2*np.sqrt(2))
            # The third order term is set to 1% of the coupling strength. This is the case if L_A ~ 2 L_T and w_A=10GHz, W_T=0.5GHz
            if self.q3 == "q3":
                self.Hamiltonian['q'] += self.q3_strength * self.g0 * ((a.dag() + a)**3) / np.sqrt(2**3)
            elif self.q3 == "q3_adag3_a3":
                self.Hamiltonian['q'] += self.q3_strength * self.g0 * ((a.dag() + a)**3-a.dag()**3-a**3) / np.sqrt(2**3)

        if self.time_dependent:
            prefactor = "kerr * np.sign(sin(omega * t / 2)) * abs(sin(omega * t))"

            if self.kerr_type == 'ancilla':
                self.mesolve_args = {"kerr": self.kerr_strength, "omega": self.drive_freq}
                H1 = (b.dag() * b )**2
            elif self.kerr_type == 'target':
                self.mesolve_args = {"kerr": self.kerr_strength, "omega": self.drive_freq}
                H1 = (a.dag() * a )**2
            elif self.kerr_type == 'cross':
                self.mesolve_args = {"kerr": self.kerr_strength, "omega": self.drive_freq}
                H1 = (a.dag() * a ) * (b.dag() * b)
            elif self.kerr_type == 'full':
                # pylint disable=unsubscriptable-object
                assert len(self.kerr_strength) == 3
                self.mesolve_args = {"kerr": 1., "omega": self.drive_freq}
                H1 = self.kerr_strength[0]*((b.dag() * b )**2 + (b.dag() * b ))
                H1 += self.kerr_strength[1]*((a.dag() * a )**2 + (a.dag() * a ))
                H1 += self.kerr_strength[2]*(2*(a.dag() * a ) * (b.dag() * b)+(a.dag() * a )+(b.dag() * b ))
            else:
                raise NotImplementedError

            for quadrature in ('q', 'p'):
                self.Hamiltonian[quadrature] = [self.Hamiltonian[quadrature], [H1, prefactor]]

        if self.time_dependent:
            self.mesolve_opts = qutip.Options(nsteps=100000, rhs_reuse=True)
        elif self.initial_A[0]=='disp_thermal':
            self.mesolve_opts = qutip.Options(nsteps=100000, rhs_reuse=True, num_cpus=1)
        else:
            self.mesolve_opts = qutip.Options(nsteps=100000)

        # self.ancilla_photons = expect(self.nb, self._state)
        assert self.ancilla_photons is not None
        ancilla_alpha0 = np.sqrt(self.ancilla_photons)
        # Set up worker functions for parallel computation
        self.qfunc_array = qfunc_amat(self.qvec, self.pvec, n=self.dims[1])
        self._wigner_worker = partial(wigner, xvec=self.qvec, yvec=self.pvec)
        self._husimi_worker = partial(qfunc, xvec=self.qvec, yvec=self.pvec, amat_pwr=self.qfunc_array)
        self._subsystem0 = partial(qutip.ptrace, sel=0)
        self._subsystem1 = partial(qutip.ptrace, sel=1)
        if self.measurement == 'fixed':
            proj = tensor(qeye(self.dims[0]), coherent(self.dims[1], ancilla_alpha0).proj())
            self._post_meas_worker = partial(post_measurement, alpha=None, proj=proj)

        if self.tidyup:
            self._tidyup()

    def _tidyup(self):
        self.tidyup = True
        if self._state is not None:
            self._state = self._state.tidyup()
        if self.state_list is not None:
            if self.thermal_envelope is not None:
                assert len(self.thermal_envelope) == len(self.state_list)
                thermal_envelope_ = []
                state_list_ = []
                for i, val in enumerate(self.thermal_envelope):
                    if val > 10**(-12):
                        thermal_envelope_.append(val)
                        state_list_.append(self.state_list[i])
                assert len(self.thermal_envelope) == len(self.state_list)
                self.thermal_envelope = np.array(thermal_envelope_)
                self.state_list = state_list_
            self.state_list = [state.tidyup() for state in self.state_list]
        for quadrature in ['q', 'p']:
            self.Hamiltonian[quadrature] = self.Hamiltonian[quadrature].tidyup()
        self.stabilizers = [stab.tidyup() for stab in self.stabilizers]
        self.stabilizers_full = [stab.tidyup() for stab in self.stabilizers_full]



    # properties of the two systems
    @property
    def state(self)->qutip.Qobj:
        if self._state is None:
            raise NotImplementedError
            # assert self.method in ('MC2', "MC")
            # combined_state = []
            # for state in self.state_list:
            #     s = state.unit()
            #     sd = s.dag()
            #     cs = s * sd
            #     combined_state.append(cs)
            # combined_state = sum(combined_state)
            # nrm = np.trace(combined_state)
            # combined_state /= nrm
            # self._state = combined_state
        return self._state

    @state.setter
    def state(self, psi: qutip.Qobj)->None:
        self._state = psi

    def subsystem(self, idx: int)->qutip.Qobj:
        """Trace out either ancilla or target, return the other

        Arguments:
            idx {int} -- Which system to return:
                            Target: 0, Ancilla: 1

        Returns:
            qutip.Qobj -- state after tracing out
        """
        state = self.state
        return state.ptrace(idx).unit()

    def photons(self):
        """Return the number of photons (and variance) for both oscillators
        """
        na = expect(self.na, self._state)
        na_err = np.sqrt(expect(self.na**2, self._state)-na)
        nb = expect(self.nb, self._state)
        nb_err = np.sqrt(expect(self.nb**2, self._state)-nb)
        return na, na_err, nb, nb_err

    def effective_squeezing(self):
        """Determine the effective squeezing of both oscillators
        """
        return effective_squeezing(self.state, self.stabilizers_full, self.spacing)

    def entropy(self):
        if self.method == 'MC2':
            with concurrent.futures.ProcessPoolExecutor() as executor:
                e = list(executor.map(self.entropy_single, self.state_list))
            return np.average(e, axis=0)
        else:
            return np.asarray(self.entropy_single(self.state))

    def entropy_single(self, psi: qutip.Qobj) -> Tuple[float, float]:
        return qutip.entropy_linear(psi.ptrace(0)), qutip.entropy_linear(psi.ptrace(1))

    def purity(self):
        return 1 - self.entropy()

    # Operations
    def displace(self, alpha: complex)->None:
        d = tensor(displace(self.dims[0], alpha), qeye(self.dims[1]))
        if self._state is None:
            assert self.method == 'MC2'
            self.state_list = list(map(lambda state: d * state, self.state_list))
        else:
            if self._state.type == 'oper':
                self._state = d * self._state * d.dag()
            elif self._state.type == 'ket':
                self._state = d * self._state
            else: raise NotImplementedError

    def update(self, quadrature: str)->None:
        """Wrapper: perform a measurement protocol (no readout) for S_q or S_p

        Arguments:
            quadrature {str} -- Quadrature to be measured
        """
        if self.method == 'MC2':
            raise NotImplementedError
            self._update_mc2(quadrature)
        elif self.method == 'MC':
            self._update_mc(quadrature)
        elif self.method == 'Liouville':
            self._update_Liouville(quadrature)
        elif self.method == "Lindblad":
            self._update_Lindblad(quadrature)
        else:
            raise NotImplementedError

    def _update_mc(self, quadrature: str)->None:
        assert self.c_ops is not None
        self.state_list = mcsolve(self.Hamiltonian[quadrature], self.state, self.t, self.c_ops, options=self.mesolve_opts, ntraj=self.ntraj, progress_bar=True).states[:,-1]
        self.state_list = [state.unit() for state in self.state_list]
        with concurrent.futures.ProcessPoolExecutor() as executor:
            states_a = parallel_map(self._subsystem1, self.state_list, executor=executor)
            self.husimi = parallel_map(self._husimi_worker, states_a, sum_up=True, executor=executor)
            self.state = parallel_map(ket2dm, self.state_list, sum_up=True, executor=executor)
        self.state = dm_norm(self.state)
        h = self.husimi
        h[np.where(h < 0)] = 0
        h = h.flatten()

        if self.measurement == 'fixed':
            q = np.sqrt(self.ancilla_photons)
            p = 0
        else:
            if self.method == 'max':
                idx = np.argmax(h)
            elif self.method == 'random':
                h /= np.sum(h)
                idx = np.random.choice(np.arange(len(h)), p=h)
            else:
                raise NotImplementedError
            idx_p, idx_q = divmod(idx, len(self.qvec))
            q, p = self.qvec[idx_q], self.pvec[idx_p]

        self.result = q, p
        self.state = post_measurement(self.state, (q+ 1j*p)/np.sqrt(2))

    def _update_mc2(self, quadrature: str)->None:
        evo = partial(self.__evolution_mc2, quadrature=quadrature)
        with concurrent.futures.ProcessPoolExecutor() as executor:
            self.state_list = list(executor.map(evo, self.state_list))
            self.state = None
            if self.mc_combined:
                q, p, husimi = heterodyne(self.subsystem(1), self.qvec, self.pvec, amat_pwr=self.qfunc_array, method=self.measurement, full_output=True)
                self.husimi = husimi
                projector = tensor(qeye(self.dims[0]), coherent(self.dims[1], (q+ 1j*p)/np.sqrt(2)).proj())
                self.state_list = list(map(lambda state: projector * state, self.state_list))
            else:
                raise NotImplementedError
            self.state = None

    def __evolution_mc2(self, psi:qutip.Qobj, quadrature: str)->qutip.Qobj:
        opts = qutip.Options(nsteps=100000, ntraj=1, num_cpus=1, rhs_reuse=True)
        result = qutip.mcsolve(self.Hamiltonian[quadrature], psi, self.t, self.c_ops, options=opts,progress_bar=False)
        if self.kappa_T is None:
            assert self.kappa_A is None
            # If there are no collapse operators, qutip returns a qobj
            psi = result.states[-1]
        else:
            # If there are collapse operators, qutip returns a list of qobj, even if there is only one traj
            psi = result.states[-1][-1]
        return psi

    def _update_Liouville(self, quadrature: str)->None:
        state = operator_to_vector(self.state)
        state *= self.time_evo[quadrature]
        state = vector_to_operator(state)
        q, p, self.husimi = heterodyne(self.subsystem(1), self.qvec, self.pvec, amat_pwr=self.qfunc_array, method=self.measurement, full_output=True)
        self.state = post_measurement(self.state, (q+ 1j*p)/np.sqrt(2))

    def _update_Lindblad(self, quadrature: str)->None:
        self.state = mesolve(self.Hamiltonian[quadrature], self.state, self.t, self.c_ops, options=self.mesolve_opts, args=self.mesolve_args, progress_bar=True).states[-1]
        if self.measurement == 'fixed':
            q = np.sqrt(self.ancilla_photons)
            p = 0
            if self.thermal_meas is not None:
                w_a = wigner(self.subsystem(1), self.qvec, self.pvec)
                w_th = wigner(thermal_dm(self.dims[1], self.thermal_meas), self.qvec, self.pvec)
                h = scipy.signal.fftconvolve(w_a, w_th, mode='same')
                h[np.where(h<0)] = 0
                h /= np.sum(h)
                self.husimi = h
            else:
                self.husimi = qfunc(self.subsystem(1), self.qvec, self.pvec, amat_pwr=self.qfunc_array)
        else:
            if self.thermal_meas is not None:
                w_a = wigner(self.subsystem(1), self.qvec, self.pvec)
                w_th = wigner(thermal_dm(self.dims[1], self.thermal_meas), self.qvec, self.pvec)
                h = scipy.signal.fftconvolve(w_a, w_th, mode='same')
                h[np.where(h<0)] = 0
                h /= np.sum(h)
                self.husimi = h
                self.husimi_p = h.flatten()
                idx = np.random.choice(np.arange(len(self.husimi_p)), p=self.husimi_p)
                idx_p, idx_q = divmod(idx, len(self.qvec))
                q, p = self.qvec[idx_q], self.pvec[idx_p]
            else:
                q, p, self.husimi = heterodyne(self.subsystem(1), self.qvec, self.pvec, amat_pwr=self.qfunc_array, method=self.measurement, full_output=True)
        self.result = q, p
        self.state = post_measurement(self.state, (q+ 1j*p)/np.sqrt(2))

    # Prepare data for plots
    def anim(self, quadrature: str)->None:
        states = mesolve(self.Hamiltonian[quadrature], self.state, self.t, self.c_ops, options=qutip.Options(nsteps=100000)).states
        with concurrent.futures.ProcessPoolExecutor() as executor:
            with tqdm(total=8) as t:
                target_states = parallel_map(self._subsystem0, states, executor)
                t.update()
                states_p = parallel_map(self._post_meas_worker, states, executor)
                t.update()
                target_states_p = parallel_map(self._subsystem0, states_p, executor)
                t.update()
                ancilla_states = parallel_map(self._subsystem1, states, executor)
                t.update()

                target_w = parallel_map(self._wigner_worker, target_states, executor)
                t.update()
                target_p_w = parallel_map(self._wigner_worker, target_states_p, executor)
                t.update()
                ancilla_w = parallel_map(self._wigner_worker, ancilla_states, executor)
                t.update()
                ancilla_h = parallel_map(self._husimi_worker, ancilla_states, executor)
                t.update()
        return target_w, target_p_w, ancilla_w, ancilla_h

    # Plotting functions
    def plot(self, ax, wmap: bool = False, husimi: bool = True)->None:
        for i in range(2):
            w = wigner(self.subsystem(i), self.qvec, self.pvec, method='clenshaw')
            if wmap:
                wmap = wigner_cmap(w)
                ax[i].imshow(w, cmap=wmap, origin="lower",extent=(-self.w_range, self.w_range, -self.w_range, self.w_range))
            else:
                ax[i].imshow(w, origin="lower",extent=(-self.w_range, self.w_range, -self.w_range, self.w_range))
            titles = ["Target", "Ancilla"]
            ax[i].set_title(titles[i])
            ax[i].set_xlabel('q')
            ax[i].set_ylabel('p')
        if husimi:
            assert self.husimi is not None
            ax[2].imshow(self.husimi, origin="lower",extent=(-self.w_range, self.w_range, -self.w_range, self.w_range))
            ax[2].set_title(r'$P(\beta)$')
            ax[2].set_xlabel('q')
            ax[2].set_ylabel('p')
        else:
            h = np.zeros((len(self.qvec), len(self.pvec)))
            ax[2].imshow(h, origin="lower",extent=(-self.w_range, self.w_range, -self.w_range, self.w_range))

    def plot_line(self, fig, ax, subsystem: str, wmap: bool=False, colorbar: bool=False, title=None)->None:
        idx_dict = {"Target":0, "Ancilla":1}

        if subsystem in ("Target", "Ancilla"):
            i = idx_dict[subsystem]
            w = wigner(self.subsystem(i), self.qvec, self.pvec, method='clenshaw')
            #w = np.around(w, decimals=3)

            norm = matplotlib.colors.Normalize(-w.real.max(), w.real.max())
            cmap = plt.get_cmap('seismic_r')

            if self.spacing == 2*np.sqrt(np.pi):
                extent = self.w_range/np.sqrt(np.pi)
                ax.set_xlabel(r'$q/\sqrt{\pi}$')
                ax.set_ylabel(r'$p/\sqrt{\pi}$')
                ax.set_xticks(np.arange(-3, 4, 1))
                ax.set_yticks(np.arange(-3, 4, 1))
            elif self.spacing == np.sqrt(2*np.pi):
                extent = self.w_range/(np.sqrt(2 * np.pi))
                ax.set_xlabel(r'$q/\sqrt{2\pi}$')
                ax.set_ylabel(r'$p/\sqrt{2\pi}$')
                ax.set_xticks(np.arange(-2, 3, 1))
                ax.set_yticks(np.arange(-2, 3, 1))

            im = ax.imshow(w, norm=norm, cmap=cmap, origin="lower", extent=(-extent, extent, -extent, extent))
            divider = make_axes_locatable(ax)
            cax = divider.append_axes("right", size="5%", pad=0.05)
            fig.colorbar(im, cax=cax)

            if title is not None:
                ax.set_title(title)
            else:
                ax.set_title(subsystem)

            ax.axhline(linewidth=0.1, color='k')
            ax.axvline(linewidth=0.1, color='k')

        elif subsystem == "Husimi":
            assert self.husimi is not None
            extent = self.w_range/np.sqrt(np.pi)
            norm=matplotlib.colors.Normalize(-self.husimi.real.max(), self.husimi.real.max())
            cmap=plt.get_cmap('seismic_r')
            im = ax.imshow(self.husimi, norm=norm, cmap=cmap, origin="lower", extent=(-extent, extent, -extent, extent))
            divider = make_axes_locatable(ax)
            cax = divider.append_axes("right", size="5%", pad=0.05)
            fig.colorbar(im, cax=cax)
            ax.scatter(self.result[0]/np.sqrt(np.pi),self.result[1]/np.sqrt(np.pi), color="y", marker='x')

            ax.set_title(r'$P(\beta)$')
            ax.set_xlabel(r'$q/\sqrt{\pi}$')
            ax.set_ylabel(r'$p/\sqrt{\pi}$')
            ax.axhline(linewidth=0.1, color='k')
            ax.axvline(linewidth=0.1, color='k')
            ax.set_xticks(np.arange(-3, 4, 1))
            ax.set_yticks(np.arange(-3, 4, 1))
        else:
            raise NotImplementedError

        #if colorbar:
        #    fig.subplots_adjust(right=0.8)
        #    cbar_ax = fig.add_axes([0.85, 0.15, 0.05, 0.7])
        #    fig.colorbar(im, cax=cbar_ax)

    def plot_fock(self, fig, ax)->None:
        for i in range(2):
            plot_fock_distribution(self.subsystem(i), fig=fig, ax=ax[i], title=f"{i}")

class Samples(System):
    def pre_measurement(self, quadrature: str)->None:
        if self.tidyup:
            self._tidyup()
        if self.method == 'Lindblad':
            self._pre_Lindblad(quadrature)
        elif self.method == 'Lindblad2':
            self._pre_Lindblad2(quadrature)
        elif self.method == "MC":
            self._pre_MC(quadrature)
        else:
            raise NotImplementedError
        # elif self.method == 'MC2':
            # raise NotImplementedError
            # assert self.c_ops is not None
            # res = mcsolve(self.Hamiltonian[quadrature], self.state, self.t, self.c_ops, options=qutip.Options(nsteps=100000), ntraj=self.ntraj, progress_bar=True).states
            # state_chunks = chunks(res, 10)
            # with concurrent.futures.ProcessPoolExecutor() as executor:
            #     res = list(executor.map(dm, state_chunks))
            # res = sum(res)
            # res /= res.tr()
            # self.state = res
            # self.husimi_p = qfunc(self.subsystem(1), self.qvec, self.pvec, amat_pwr=self.qfunc_array)
        self.husimi_p[np.where(self.husimi_p<0)] = 0
        self.husimi_p = self.husimi_p.flatten()
        self.husimi_p /= np.sum(self.husimi_p)
        if self.tidyup:
            self._tidyup()

    def _pre_Lindblad(self, quadrature: str)->None:
        self.state = mesolve(self.Hamiltonian[quadrature], self.state, self.t, self.c_ops, options=self.mesolve_opts, args=self.mesolve_args, progress_bar=True).states[-1]
        if self.thermal_meas is None:
            self.husimi_p = qfunc(self.subsystem(1), self.qvec, self.pvec, amat_pwr=self.qfunc_array)
        else:
            w_a = wigner(self.subsystem(1), self.qvec, self.pvec)
            w_th = wigner(thermal_dm(self.dims[1], self.thermal_meas), self.qvec, self.pvec)
            h = scipy.signal.fftconvolve(w_a, w_th, mode='same')
            self.husimi_p = h

    def _pre_Lindblad2(self, quadrature: str)->None:
        assert self._state is None
        assert self.c_ops == []
        assert isket(self.state_list[0])
        mesolv = partial(mesolve, self.Hamiltonian[quadrature], tlist=self.t, c_ops=self.c_ops, options=self.mesolve_opts)
        with concurrent.futures.ProcessPoolExecutor() as executor:
            result_list = parallel_map(mesolv, self.state_list, executor=executor)
            self.state_list = [result.states[-1].unit() for result in result_list]

            states_a = parallel_map(self._subsystem1, self.state_list, executor=executor)
            self.husimi_p = parallel_map(self._husimi_worker, states_a, sum_up=self.thermal_envelope, executor=executor)
        self.state = parallel_map(ket2dm, self.state_list, sum_up=self.thermal_envelope)
        self.state = dm_norm(self.state)

    def _pre_MC(self, quadrature: str)->None:
        assert self.c_ops is not None
        self.state_list = mcsolve(self.Hamiltonian[quadrature], self.state, self.t, self.c_ops, options=self.mesolve_opts, ntraj=self.ntraj, progress_bar=True).states[:,-1]
        self.state_list = [state.unit() for state in self.state_list]
        with concurrent.futures.ProcessPoolExecutor() as executor:
            states_a = parallel_map(self._subsystem1, self.state_list, executor=executor)
            self.husimi_p = parallel_map(self._husimi_worker, states_a, sum_up=True, executor=executor)
            self.state = parallel_map(ket2dm, self.state_list, sum_up=True, executor=executor)
        self.state = dm_norm(self.state)

    def sample(self, *_)->Tuple[float, float, float, complex]:
        """Return key metrics for a single, random measurement result
           To be used after preparing the pre-measurement state with pre_measurement.

        Arguments:
            _ -- NOT USED, throwaway to enable combination with a map

        Returns:
            Tuple[float, float, float, complex] -- Delta_p, Delta_q, mean photons in target, measurement result
        """
        idx = np.random.choice(np.arange(len(self.husimi_p)), p=self.husimi_p)
        idx_p, idx_q = divmod(idx, len(self.qvec))
        q, p = self.qvec[idx_q], self.pvec[idx_p]
        beta = (q+ 1j*p)/np.sqrt(2)
        psi = post_measurement(self.state, beta, unit=True, thermal=self.thermal_meas, tidyup=self.tidyup)
        na = expect(self.na, psi)
        delta_p, delta_q = effective_squeezing(psi, self.stabilizers_full, self.spacing) # pylint: disable=unbalanced-tuple-unpacking
        return delta_p, delta_q, na, beta



# -----------------------------------------------------------------------------
# Q FUNCTION
#

def qfunc(state:qutip.Qobj, xvec:np.array, yvec:np.array, g:float=sqrt(2), amat_pwr:Optional[np.array]=None)->np.array:
    """Q-function of a given state vector or density matrix
    at points `xvec + i * yvec`.

    Parameters
    ----------
    state : qobj
        A state vector or density matrix.

    xvec : array_like
        x-coordinates at which to calculate the Wigner function.

    yvec : array_like
        y-coordinates at which to calculate the Wigner function.

    g : float
        Scaling factor for `a = 0.5 * g * (x + iy)`, default `g = sqrt(2)`.

    amat_pwr : array
        The matrix provided by qfunc_amat(xvec, yvec, n, g), where xvec, yvec and g
        need to be the same as for qfunc, and n = np.prod(state.shape) is the dim
        of a pure state in the chosen system.
        If provided, the pure qfunc is computed with _qfunc_pure_fast, which has
        significant speedup over _qfunc_pure. Useful if Q is computed for density
        matrices or many times.

    Returns
    --------
    Q : array
        Values representing the Q-function calculated over the specified range
        [xvec,yvec].

    """
    X, Y = meshgrid(xvec, yvec)
    amat = 0.5 * g * (X + Y * 1j)

    if not (isoper(state) or isket(state)):
        raise TypeError('Invalid state operand to qfunc.')

    qmat = zeros(size(amat))

    if isket(state):
        qmat = _qfunc_pure(state, amat, amat_pwr)
    elif isoper(state):
        d, v = la.eig(state.full())
        # d[i]   = eigenvalue i
        # v[:,i] = eigenvector i

        qmat = zeros(np.shape(amat))
        for k in arange(0, len(d)):
            qmat1 = _qfunc_pure(v[:, k], amat, amat_pwr)
            qmat += real(d[k] * qmat1)

    qmat = 0.25 * qmat * g ** 2
    return qmat

#
# Q-function for a pure state: Q = |<alpha|psi>|^2 / pi
#
# |psi>   = the state in fock basis
# |alpha> = the coherent state with amplitude alpha
#
def _qfunc_pure(psi:qutip.Qobj, alpha_mat: np.array, amat_pwr=None):
    """
    Calculate the Q-function for a pure state.

    If provided, amat_pwr needs to be computed with qfunc_amat(xvec, yvec, n, g), where xvec, yvec and g
    need to be the same as for qfunc, and n = np.prod(state.shape) is the dim
    of a pure state in the chosen system. This gives 3-10x speedup for each call
    """
    n = np.prod(psi.shape)
    if isinstance(psi, Qobj):
        psi = psi.full().flatten()
    else:
        psi = psi.T

    if amat_pwr is None:
        qmat = polyval(fliplr([psi / sqrt(factorial(arange(n)))])[0],
                           conjugate(alpha_mat))
    else:
        qmat = np.dot(amat_pwr, psi)
    # faster than np.abs()**2 if len(xvec) >~ 10
    qmat = qmat.real**2 + qmat.imag**2
    if amat_pwr is None:
        qmat *= exp(-abs(alpha_mat) ** 2)
    return qmat / pi

def qfunc_amat(xvec: np.array, yvec: np.array, n: int, g:float=sqrt(2))->np.array:
    """Helper matrix for fast Q-function at points `xvec + i * yvec`.

    Warning: The returned array has size len(xvec) * len(yvec) * n, can be large

    Parameters
    ----------
    xvec : array_like
        x-coordinates at which to calculate the Wigner function.

    yvec : array_like
        y-coordinates at which to calculate the Wigner function.

    g : float
        Scaling factor for `a = 0.5 * g * (x + iy)`, default `g = sqrt(2)`.

    Returns
    --------
    amat_pwr : array
        Precomputed array that contains everything in _qfunc_pure that is not dependent on
        the state.

    """
    X, Y = meshgrid(xvec, yvec)
    amat = 0.5 * g * (X - Y * 1j)

    powers = np.arange(n)
    amat_pwr = np.power(np.expand_dims(amat, axis=-1), powers)
    amat_pwr /= sqrt(factorial(arange(n)))
    amat_pwr *= np.expand_dims(exp(-abs(amat) ** 2 / 2), axis=-1)
    return amat_pwr

def heterodyne(psi: qutip.Qobj, qvec:np.array, pvec:np.array, amat_pwr=None,
                method:str='random', full_output:bool=False) -> Union[Tuple[float, float], Tuple[float, float, np.array]]:
    if pvec is None:
        pvec = qvec
    h = qfunc(psi, qvec, pvec, amat_pwr=amat_pwr)

    h[np.where(h < 0)] = 0
    husimi = h.flatten()

    if method == 'max':
        idx = np.argmax(husimi)
    elif method == 'random':
        husimi /= np.sum(husimi)
        idx = np.random.choice(np.arange(len(husimi)), p=husimi)
    else:
        raise NotImplementedError
    idx_p, idx_q = divmod(idx, len(qvec))

    if full_output:
        return qvec[idx_q], pvec[idx_p], h
    else:
        return qvec[idx_q], pvec[idx_p]

def post_measurement(psi: qutip.Qobj, alpha: complex, unit: bool = True, thermal=None, proj=None, tidyup=False) -> qutip.Qobj:
    """generate the post-measurement state, given a measurement result

    Arguments:
        psi {qutip.Qobj} -- initial state
        alpha {complex} -- measurement result

    Keyword Arguments:
        unit {bool} -- wether the output is normalized (default: {True})
        thermal {real} -- Nr of thermal photons in the projector, simulates imperfect measurement.
                            If None, an ideal measurement is made (project onto coherent state) (default: {None})
        proj {qutip.Qobj} -- If given, this will be the projector applied. Useful for fixed measurement results (default: {None})

    Returns:
        qutip.Qobj -- post-measurement state
    """
    assert len(psi.dims[0]) == 2, "wrong dimensions"
    assert psi.isket or psi.isoper, "not a qobj"
    N_A = psi.dims[0][0]
    N_B = psi.dims[0][1]
    if proj is None:
        if thermal is None:
            coh = coherent(N_B, alpha)
            if tidyup:
                coh = coh.tidyup()
            projector = tensor(qeye(N_A), coh.proj())
            if tidyup:
                projector = projector.tidyup()
        else:
            d = displace(N_B, alpha)
            if tidyup:
                d = d.tidyup()
            th = d * thermal_dm(N_B, thermal) * d.conj()
            if tidyup:
                th = th.tidyup()
            projector = tensor(qeye(N_A), th)
            if tidyup:
                projector = projector.tidyup()
    else:
        assert alpha is None, "projector and result both given"
        projector = proj

    if psi.isoper:
        psi = projector * psi * projector
        if unit:
            psi = dm_norm(psi)
    elif psi.isket:
        psi = projector * psi
        if unit:
            return psi.unit()
    return psi

def effective_squeezing(psi:qutip.Qobj, stabilizers:qutip.Qobj, spacing:float)-> List[float]:
    delta = []
    for S in stabilizers:
        d = expect(S, psi)
        if isinstance(psi, (list,np.ndarray)):
            d = np.average(d)
        d = np.sqrt(-4 * np.log(np.abs(d))) / spacing
        delta.append(d)
    return delta

def chunks(l, n):
    """cut a list l into chunks of length n

    Arguments:
        l {list} -- list to be split
        n {int} -- length of the chunks
    """
    # For item i in a range that is a length of l,
    for i in range(0, len(l), n):
        # Create an index range for l of n items:
        yield l[i:i+n]

# def dm(states):
#     out = qutip.ket2dm(qutip.Qobj(dims=states[0][0].dims))
#     for state_row in states:
#         s = state_row[-1].unit()
#         s = qutip.ket2dm(s)
#         out += s
#     out /= out.tr()
#     return out

def ket2dm(state: qutip.Qobj) -> qutip.Qobj:
    """Take a ket and return a density matrix
       WORKAROUND for the MKL bug (appears in unit() method of Qobj)

    Arguments:
        state {qutip.Qobj} -- state vector, must be ket

    Returns:
        qutip.Qobj -- same state as density matrix
    """
    assert state.isket
    s = state.unit()
    return s * s.dag()

def dm_norm(state: qutip.Qobj) -> qutip.Qobj:
    """Normalize a qutip operator
       WORKAROUND for the MKL bug (appears in unit() method of Qobj)

    Arguments:
        state {qutip.Qobj} -- state, must be density matrix

    Returns:
        qutip.Qobj -- normalized state
    """
    assert state.isoper
    nrm = state.tr()
    state /= nrm
    return state

def parallel_map(function: Callable[[Any], Any], iterable: Iterable, executor:Optional[concurrent.futures.ProcessPoolExecutor]=None,
                 chunksize: int=1, max_workers: Optional[int]=None, sum_up:Union[bool, Iterable]=False):
    """Parallel version of map(function, iterable).
       Wrapper used to initialize the Pool, if it is not given

    Arguments:
        function {Callable[[Any], Any]} -- function to be mapped
        iterable {Iterable} -- iterable the function should be applied to

    Keyword Arguments:
        executor {Optional[concurrent.futures.ProcessPoolExecutor]} -- A ProcessPoolExecutor can be given for repeated use (default: {None})
        chunksize {int} -- Send items to the pool in chunks instead of one-by-one. Can speed up (default: {1})
        max_workers {Optional[int]} -- Limit the number of worker processes. Useful if memory is a concern. (default: All logical cores)
        sum_up {Union[bool, Iterable]} -- If true, the results of the map are summed up (default: {False}).
                                          If an Iterable of equal length as iterable is given, it will be used as weight
    """
    if not isinstance(sum_up, bool):
        assert len(iterable) == len(sum_up)

    if executor is None:
        with concurrent.futures.ProcessPoolExecutor(max_workers=max_workers) as executor:
            output = _parallel_map(function, iterable, executor, chunksize, sum_up)
    else:
        assert max_workers is None
        output = _parallel_map(function, iterable, executor, chunksize, sum_up)
    return output

def _parallel_map(function: Callable[[Any], Any], iterable: Iterable, executor:concurrent.futures.ProcessPoolExecutor, chunksize: int, sum_up: Union[bool, Iterable]):
    """Parallel version of map(function, iterable).

    Arguments:
        function {Callable[[Any], Any]} -- function to be mapped
        iterable {Iterable} -- iterable the function should be applied to

    Keyword Arguments:
        executor {concurrent.futures.ProcessPoolExecutor} -- A ProcessPoolExecutor can be given for repeated use
        chunksize {int} -- Send items to the pool in chunks instead of one-by-one. Can speed up
        sum_up {bool} -- If true, the results of the map are summed up
    """
    if chunksize == 1:
        output = []
        if sum_up is False:
            futures = [executor.submit(_enumerate_f, counter, item, function) for counter, item in enumerate(iterable)]
            for f in tqdm(as_completed(futures), total=len(iterable)):
                output.append(f.result())
            output = [s[1] for s in sorted(output, key=lambda x: x[0])]
        elif sum_up is True:
            futures = [executor.submit(function, item) for item in iterable]
            t = tqdm(total=len(iterable))
            generator = as_completed(futures)
            output = next(generator).result()
            t.update()
            for f in generator:
                temp = f.result()
                output += temp
                t.update()
        else:
            assert len(sum_up) == len(iterable)
            futures = [executor.submit(_enumerate_f, counter, item, function) for counter, item in enumerate(iterable)]

            t = tqdm(total=len(iterable))
            generator = as_completed(futures)
            idx, temp = next(generator).result()
            output = sum_up[idx] * temp
            t.update()
            for f in generator:
                idx, temp = f.result()
                output += sum_up[idx] * temp
                t.update()
    else:
        assert not sum_up
        output = list(executor.map(function, iterable, chunksize=chunksize))
    return output

def serial_map(function: Callable[[Any], Any], iterable: Iterable, sum_up: Union[bool, Iterable]):
    """version of map(function, iterable) with progress bar and summing up

    Arguments:
        function {Callable[[Any], Any]} -- function to be mapped
        iterable {Iterable} -- iterable the function should be applied to

    Keyword Arguments:
        sum_up {bool} -- If true, the results of the map are summed up
    """
    if sum_up is False:
        output = [function(item) for item in tqdm(iterable)]
    if sum_up is True:
        def _map(function, iterable):
            for item in iterable:
                yield function(item)
        output = sum(tqdm(_map(function, iterable), total=len(iterable)))
    else:
        assert len(sum_up) == len(iterable)
        def _map(function, iterable, sum_up):
            for i, item in enumerate(iterable):
                yield sum_up[i] * function(item)
        output = sum(tqdm(_map(function, iterable, sum_up), total=len(iterable)))
    return output

def _enumerate_f(counter:int, item: Any, function: Callable) -> Tuple[int, Any]:
    """ Apply function to item, return with the label counter.
        Helper for _parallel_map to get ordered returns from an asynchronous map

    Arguments:
        counter {int} -- Label used to sort results
        item -- items in the iterable over which the map is performed
        function {Callable} -- function that is mapped
    """
    return (counter, function(item))

def low_priority() -> None:
    """Set the priority of the calling process to low
    """
    if psutil.WINDOWS:
        nice = psutil.IDLE_PRIORITY_CLASS
    elif psutil.MACOS:
        nice = 19
    process = psutil.Process(os.getpid())
    process.nice(nice)
    #print(process.ionice())
    #process.ionice(psutil.IOPRIO_VERYLOW)
