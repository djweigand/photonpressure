import numpy as np
from scipy import (arange, exp, pi, zeros, size, polyval, fliplr, conjugate,
                   sqrt, meshgrid, real)

import scipy.linalg as la
from qutip.qobj import Qobj, isket, isoper
from scipy.misc import factorial
import qutip
from qutip import (tensor, qeye, coherent, expect)

from typing import List, Tuple
# -----------------------------------------------------------------------------
# Q FUNCTION
#


def qfunc(state:qutip.Qobj, xvec, yvec, g=sqrt(2), amat_pwr=None)->np.array:
    """Q-function of a given state vector or density matrix
    at points `xvec + i * yvec`.

    Parameters
    ----------
    state : qobj
        A state vector or density matrix.

    xvec : array_like
        x-coordinates at which to calculate the Wigner function.

    yvec : array_like
        y-coordinates at which to calculate the Wigner function.

    g : float
        Scaling factor for `a = 0.5 * g * (x + iy)`, default `g = sqrt(2)`.

    amat_pwr : array
        The matrix provided by qfunc_amat(xvec, yvec, n, g), where xvec, yvec and g
        need to be the same as for qfunc, and n = np.prod(state.shape) is the dim
        of a pure state in the chosen system.
        If provided, the pure qfunc is computed with _qfunc_pure_fast, which has
        significant speedup over _qfunc_pure. Useful if Q is computed for density
        matrices or many times.

    Returns
    --------
    Q : array
        Values representing the Q-function calculated over the specified range
        [xvec,yvec].

    """
    X, Y = meshgrid(xvec, yvec)
    amat = 0.5 * g * (X + Y * 1j)

    if not (isoper(state) or isket(state)):
        raise TypeError('Invalid state operand to qfunc.')

    qmat = zeros(size(amat))

    if isket(state):
        qmat = _qfunc_pure(state, amat, amat_pwr)
    elif isoper(state):
        d, v = la.eig(state.full())
        # d[i]   = eigenvalue i
        # v[:,i] = eigenvector i

        qmat = zeros(np.shape(amat))
        for k in arange(0, len(d)):
            qmat1 = _qfunc_pure(v[:, k], amat, amat_pwr)
            qmat += real(d[k] * qmat1)

    qmat = 0.25 * qmat * g ** 2
    return qmat

#
# Q-function for a pure state: Q = |<alpha|psi>|^2 / pi
#
# |psi>   = the state in fock basis
# |alpha> = the coherent state with amplitude alpha
#
def _qfunc_pure(psi:qutip.Qobj, alpha_mat, amat_pwr=None):
    """
    Calculate the Q-function for a pure state.

    If provided, amat_pwr needs to be computed with qfunc_amat(xvec, yvec, n, g), where xvec, yvec and g
    need to be the same as for qfunc, and n = np.prod(state.shape) is the dim
    of a pure state in the chosen system. This gives 3-10x speedup for each call
    """
    n = np.prod(psi.shape)
    if isinstance(psi, Qobj):
        psi = psi.full().flatten()
    else:
        psi = psi.T

    if amat_pwr is None:
        qmat = polyval(fliplr([psi / sqrt(factorial(arange(n)))])[0],
                           conjugate(alpha_mat))
    else:
        qmat = np.dot(amat_pwr, psi)
    # faster than np.abs()**2 if len(xvec) >~ 10
    qmat = qmat.real**2 + qmat.imag**2
    if amat_pwr is None:
        qmat *= exp(-abs(alpha_mat) ** 2)
    return qmat / pi

def qfunc_amat(xvec: np.array, yvec: np.array, n: int, g:float=sqrt(2))->np.array:
    """Helper matrix for fast Q-function at points `xvec + i * yvec`.

    Warning: The returned array has size len(xvec) * len(yvec) * n, can be large

    Parameters
    ----------
    xvec : array_like
        x-coordinates at which to calculate the Wigner function.

    yvec : array_like
        y-coordinates at which to calculate the Wigner function.

    g : float
        Scaling factor for `a = 0.5 * g * (x + iy)`, default `g = sqrt(2)`.

    Returns
    --------
    amat_pwr : array
        Precomputed array that contains everything in _qfunc_pure that is not dependent on
        the state.

    """
    X, Y = meshgrid(xvec, yvec)
    amat = 0.5 * g * (X - Y * 1j)

    powers = np.arange(n)
    amat_pwr = np.power(np.expand_dims(amat, axis=-1), powers)
    amat_pwr /= sqrt(factorial(arange(n)))
    amat_pwr *= np.expand_dims(exp(-abs(amat) ** 2 / 2), axis=-1)
    return amat_pwr



def heterodyne(psi: qutip.Qobj, qvec:np.array, pvec:np.array, amat_pwr=None,
                method:str='random', full_output:bool=False) -> tuple:
    if pvec is None:
        pvec = qvec
    h = qfunc(psi, qvec, pvec, amat_pwr=amat_pwr)

    h[np.where(h<0)] = 0
    husimi = h.flatten()

    if method == 'max':
        idx = np.argmax(husimi)
    elif method == 'random':
        husimi /= np.sum(husimi)
        idx = np.random.choice(np.arange(len(husimi)), p=husimi)
    else:
        raise NotImplementedError
    idx_p, idx_q = divmod(idx, len(qvec))

    if full_output:
        return qvec[idx_q], pvec[idx_p], h
    else:
        return qvec[idx_q], pvec[idx_p]


def post_measurement(psi: qutip.Qobj, alpha: complex, unit: bool=True) -> qutip.Qobj:
    assert len(psi.dims[0]) == 2
    assert psi.isket or psi.isoper
    N_A = psi.dims[0][0]
    N_B = psi.dims[0][1]
    projector = tensor(qeye(N_A), coherent(N_B, alpha).proj())
    if psi.isoper:
        psi = projector * psi * projector
    elif psi.isket:
        psi = projector * psi
    if unit:
        return psi.unit()
    else:
        return psi

def effective_squeezing(psi:qutip.Qobj, stabilizers:qutip.Qobj, spacing:float)-> List[float]:
    delta = []
    for S in stabilizers:
        d = expect(S, psi.unit())
        d = np.sqrt(-4 * np.log(np.abs(d))) / spacing
        delta.append(d)
    return delta