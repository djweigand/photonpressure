#%%
import matplotlib
import matplotlib.pyplot as plt
import numpy as np

#from functions import low_priority
#low_priority()

matplotlib.rcParams['mathtext.fontset'] = 'cm'
matplotlib.rcParams['axes.labelsize'] = 14
matplotlib.rc('text', usetex=True)
matplotlib.rcParams['xtick.labelsize'] = 14
matplotlib.rcParams['ytick.labelsize'] = 14
matplotlib.rcParams['legend.fontsize'] = 14
#matplotlib.rcParams['figure.frameon'] = False


colors = plt.cm.viridis(np.linspace(0, 1, 4))

#%%
def drive(time, delta, s):
    return np.pi/2 + (-1)**(np.floor(time/2)+s) * np.arccos(1-delta + delta*np.cos(np.pi*time))

#%%
t = np.linspace(0, 4, 2000)
t_long = np.linspace(0, 100, 20000)
dr1_long = drive(t_long, 1, 0)
dr1 = drive(t, 1, 0)
dr05 = drive(t, 0.5, 0)
dr1m = drive(t, 1, 1)
dr05m = drive(t, 0.5, 1)

#%%

matplotlib.rcParams['mathtext.fontset'] = 'cm'
matplotlib.rcParams['axes.labelsize'] = 14
matplotlib.rc('text', usetex=True)
matplotlib.rcParams['xtick.labelsize'] = 14
matplotlib.rcParams['ytick.labelsize'] = 14
matplotlib.rcParams['legend.fontsize'] = 14
#matplotlib.rcParams['figure.frameon'] = False


fig, ax = plt.subplots(1,1, figsize=(3.4, 3.4))

(line1,) = ax.plot(t, dr1/np.pi, color=colors[0], label=r'$\delta=1$')
(line2,) = ax.plot(t, dr05/np.pi, color=colors[2], label=r'$\delta=0.4$')
#ax.plot(t, dr1m/np.pi, color=colors[0], linestyle='--')
#ax.plot(t, dr05m/np.pi, color=colors[2], linestyle='--')

ax.set_xlabel(r'$\omega_T t/\pi$')
ax.set_ylabel(r'$x_{\rm ext}(t)/\pi$')
ax.set_yticks(np.arange(-0.5, 2, 0.5))
ax.axhline(linewidth=0.7, color='k')
plt.legend((line1, line2), (r'$\delta=1$', r'$\delta=0.5$'), loc='upper right')
fig.savefig("figures/talk_drive.svg",bbox_inches='tight', pad_inches=0)

#%%
fig, ax = plt.subplots(1,1, figsize=(3.4, 3.4))
names = ['drive', 'drive_long']
times = [t, t_long]
for i, drive in enumerate([dr1, dr1_long]):
    signal = np.cos(drive)
    pos_signal = signal.copy()
    neg_signal = signal.copy()

    pos_signal[pos_signal <= 0] = np.nan
    neg_signal[neg_signal > 0] = np.nan

    plt.plot(times[i], pos_signal, color='b')
    plt.plot(times[i], neg_signal, color='r')
    ax.set_xlabel(r'$\omega_T t/\pi$')
    ax.set_yticks(np.arange(-1, 1.5, 0.5))
    ax.set_xlabel(r'$t$')
    ax.set_ylabel(r'$\cos(x_{\rm ext}(t))$')
    #plt.legend()
    fig.savefig(f"figures/talk_{names[i]}_cossin.svg",bbox_inches='tight', pad_inches=0)


# %%
