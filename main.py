import time
import warnings

import matplotlib.pyplot as plt
import numpy as np

import concurrent.futures
import functools

import qutip
from qutip import mesolve, qfunc, wigner, wigner_cmap, basis, entropy_linear, ket2dm
from qutip import operator_to_vector, vector_to_operator, liouvillian
from qutip import coherent, qeye, tensor, expect, displace, squeeze, destroy, thermal_dm, ket2dm, plot_fock_distribution

from functions import (heterodyne, post_measurement, effective_squeezing, qfunc_amat)

from typing import List, Tuple

START_TIME = time.time()

class System:
    def __init__(self, params:dict)->None:
        self.dims = params.get('dims', (200, 20))
        self.spacing = params.get('spacing', 2*np.sqrt(np.pi))
        self.kappa = params.get('kappa', None)
        self.g0 = params.get('g', 1)
        self.measurement = params.get('measurement', 'random')
        self.method = params.get('method', 'Liouville')
        assert self.method in ('Lindblad', 'Liouville', 'MC'), 'Method not defined'
        assert self.method != 'Liouville', 'Liouville needs dim^4 as memory, this is too much (TB)'
        self.units = 'stabilizer'
        ancilla_photons = params.get('photons', 3)
        alpha = np.sqrt(ancilla_photons)

        initial = params.get('initial', ('thermal', 10))
        #self.state = tensor(coherent(self.dims[0], 0), coherent(self.dims[1], 2))
        if initial[0] == 'thermal':
            assert self.method != 'MC'
            self._state = tensor(thermal_dm(self.dims[0], initial[1]), ket2dm(coherent(self.dims[1], alpha)))
        elif initial[0] == 'squeezed':
            r = np.log(1 / initial[1])
            self._state = tensor(squeeze(self.dims[0], r) * basis(self.dims[0], 0), coherent(self.dims[1], alpha))
        elif initial[0] == 'coherent':
            self._state = tensor(basis(self.dims[0], initial[1]), coherent(self.dims[1], alpha))
        else:
            raise NotImplementedError("Valid arguments: ('thermal', photons); ('squeezed', delta); ('coherent', alpha)")

        a = tensor(destroy(self.dims[0]), qeye(self.dims[1]))
        b = tensor(qeye(self.dims[0]), destroy(self.dims[1]))

        self.na = a.dag() * a
        self.nb = b.dag() * b

        if self.units == 'stabilizer':
            self.g0 *= 2*np.pi/(self.spacing)

        self.c_ops = []
        if self.kappa is not None:
            if self.units == 'stabilizer':
                self.kappa[0] *= 2*np.pi/(self.spacing)
                self.kappa[1] *= 2*np.pi/(self.spacing)
            self.c_ops.append(np.sqrt(self.kappa[0]) * a)
            self.c_ops.append(np.sqrt(self.kappa[1]) * b)

        self.H = {}
        self.H['q'] = self.g0 * (a.dag() + a) * b.dag() * b / np.sqrt(2)
        self.H['p'] = 0.5*1j* self.g0 * (a.dag() - a) * b.dag() * b / np.sqrt(2)
        print('Measure S_q, X_p')
        self.qvec = np.linspace(-4*np.sqrt(np.pi),4*np.sqrt(np.pi),401)
        self.pvec = np.linspace(-4*np.sqrt(np.pi),4*np.sqrt(np.pi),401)
        self.t_out = self.spacing / self.g0
        self.t = np.linspace(0, self.t_out, 2)
        self.stabilizers = [displace(self.dims[0], self.spacing/np.sqrt(2)), displace(self.dims[0], 1j*self.spacing/np.sqrt(2))]

        self.qfunc_array = qfunc_amat(self.qvec, self.pvec, n=self.dims[1])

        if self.method == 'MC':
            self.ntraj = 100
            self.mc_combined = True
            self.mc_states = [self._state.copy() for _ in range(self.ntraj)]
        elif self.method == 'Liouville':
            self.time_evo = {}
            for quadrature in ['q', 'p']:
                L = self.t_out * liouvillian(self.H['q'], c_ops=self.c_ops)
                print(L)
                print(L.data.nnz)
                self.time_evo[quadrature] = L.expm(method='sparse')

    @property
    def state(self)->qutip.Qobj:
        if self._state is None:
            assert self.method == 'MC'
            states = map(ket2dm, self.mc_states)
            self._state = sum(states).unit()
        return self._state

    @state.setter
    def state(self, psi:qutip.Qobj)->None:
        self._state = psi

    def update(self, quadrature:str)->None:
        if self.method == 'MC':
            evo = functools.partial(self._evolution_mc, quadrature=quadrature)
            with concurrent.futures.ProcessPoolExecutor() as executor:
                self.mc_states = list(executor.map(evo, self.mc_states))
                self.state = None
                if self.mc_combined:
                    q, p, husimi = heterodyne(self.subsystem(1), self.qvec, self.pvec, amat_pwr=self.qfunc_array, method=self.measurement, full_output=True)
                    self.husimi = husimi
                    projector = tensor(qeye(self.dims[0]), coherent(self.dims[1], (q+ 1j*p)/np.sqrt(2)).proj())
                    self.mc_states = list(map(lambda state: projector * state, self.mc_states))
                else:
                    self.mc_states = list(executor.map(self._measure, self.mc_states))
                self.state = None

        elif self.method == 'Liouville':
            state = operator_to_vector(self.state)
            state *= self.time_evo[quadrature]
            state = vector_to_operator(state)
            self.state = self._measure(state)
        elif self.method == "Lindblad":
            state = mesolve(self.H[quadrature], self.state, self.t, self.c_ops, options=qutip.Options(nsteps=100000)).states[-1]
            self.state = self._measure(state)
        else:
            raise NotImplementedError

    def _evolution_mc(self, psi:qutip.Qobj, quadrature:str)->qutip.Qobj:
        opts = qutip.Options(nsteps=100000, ntraj=1, num_cpus=1, rhs_reuse=True)
        result = qutip.mcsolve(self.H[quadrature], psi, self.t, self.c_ops, options=opts,progress_bar=False)
        if self.kappa is None:
            # If there are no collapse operators, qutip returns a qobj
            psi = result.states[-1]
        else:
            # If there are collapse operators, qutip returns a list of qobj, even if there is only one traj
            psi = result.states[-1][-1]
        return psi

    def _measure(self, psi:qutip.Qobj)->qutip.Qobj:
        q, p, h = heterodyne(psi.ptrace(1), self.qvec, self.pvec, amat_pwr=self.qfunc_array, method=self.measurement, full_output=True)
        self.husimi = h
        psi = post_measurement(psi, (q+ 1j*p)/np.sqrt(2))
        return psi

    def evolution(self, quadrature:str)->None:
        raise NotImplementedError
        self._state = mesolve(self.H[quadrature], self._state, self.t, self.c_ops, options=qutip.Options(nsteps=100000)).states[-1]

    def subsystem(self, idx:int)->qutip.Qobj:
        state = self.state 
        return state.ptrace(idx).unit()

    def measurement_round(self, quadrature:str, full:bool=False)->tuple:
        raise NotImplementedError
        '''
        self._evolution_mc(quadrature)
        q, p, husimi = heterodyne(self.subsystem(1), self.qvec, self.pvec, method=self.measurement, full_output=True)
        self._state = post_measurement(self._state, (q+ 1j*p)/np.sqrt(2))
        self.husimi = husimi
        if full:
            return q, p, husimi
        else:
            return q, p
        '''

    def effective_squeezing(self):
        return effective_squeezing(self.subsystem(0), self.stabilizers, self.spacing)

    def entropy(self):
        if self.method == 'MC':
            with concurrent.futures.ProcessPoolExecutor() as executor:
                e = list(executor.map(self.entropy_single, self.mc_states))
            return np.average(e, axis=0)
        else:
            return np.asarray(self.entropy_single(self.state))

    def purity(self):
        return 1 - self.entropy()

    def entropy_single(self, psi:qutip.Qobj) -> Tuple[float, float]:
        return entropy_linear(psi.ptrace(0)), entropy_linear(psi.ptrace(1))

    def plot(self, ax, wmap:bool=False, husimi:bool=True)->None:
        title = ['Target', 'Ancilla']
        for i in range(2):
            w = wigner(self.subsystem(i), self.qvec, self.pvec, method='clenshaw')
            if wmap:
                wmap = wigner_cmap(w)
                ax[i].imshow(w, cmap=wmap, extent=[-4, 4, -4, 4], origin='lower')
            else:
                ax[i].imshow(w, extent=[-4, 4, -4, 4], origin='lower',vmin=-0.3,vmax=0.3)
            ax[i].set_title(title[i])
        if husimi:
            ax[2].imshow(self.husimi, extent=[-4, 4, -4, 4], origin='lower')
            ax[2].set_title('Husimi Q')
        else:
            if len(ax) == 2:
                pass
            else:
                h = np.zeros((len(self.qvec), len(self.pvec)))
                ax[2].imshow(h)

    def plot_fock(self, fig, ax)->None:
        title = ['Target', 'Ancilla']
        for i in range(2):
            plot_fock_distribution(self.subsystem(i), fig=fig, ax=ax[i], title=title[i], unit_y_range=False)

    def photons(self) -> Tuple[float, float]:
        na = expect(self.na, self._state)
        nb = expect(self.nb, self._state)
        return na, nb

    def displace(self, alpha:complex)->None:
        d = tensor(displace(self.dims[0], alpha), qeye(self.dims[1]))
        if self._state.type == 'oper':
            self._state = d * self._state * d.dag()
        elif self._state.type == 'ket':
            self._state = d * self._state

if __name__ == "__main__":
    warnings.warn("There is a bug in MKL crashing this script, use other BLAS versions of numpy/scipy")
    np.__config__.show()
    """
    #params = {'kappa': [0.2, 2], 'g':1}          # Units in 100 kHz
    params = {'kappa': [0.01, 0.1]}          # Units in 100 kHz
    #params = {'kappa': None}          # Units in 100 kHz
    params['dims'] = (40, 20)
    params['ancilla_photons'] = 5
    params['measurement'] = 'max'
    params['initial'] = ('coherent', 0)
    params['method'] = 'MC'

    print("Start", START_TIME-time.time())
    system = System(params)
    e = np.asarray(system.entropy())
    print("e", START_TIME-time.time())
    print(system.purity())
    system.update('q')
    print("evo1", START_TIME-time.time())
    print(system.purity())
    system.update('p')
    print("evo2", START_TIME-time.time())
    print(system.purity())
    """
    #params = {'kappa': [0.2, 2], 'g':1}          # Units in 100 kHz
    #params = {'kappa': [0.01, 0.01]}          # Units in 100 kHz
    params = {'kappa': None}          # Units in 100 kHz
    params['dims'] = (100, 20)
    params['ancilla_photons'] = 1
    params['measurement'] = 'max'
    params['initial'] = ('squeezed', 6)
    #params['initial'] = ('coherent', 0)
    params['method'] = 'Lindblad'

    wmap = False

    if False:
        fig1, ax1 = plt.subplots(3, 3, figsize=(12,12))
        fig2, ax2 = plt.subplots(3, 2, figsize=(12,8))
        print("Start", time.time()-START_TIME)
        system = System(params)
        print(system.purity())
        print(system.effective_squeezing())
        system.plot(ax1[0], husimi=False, wmap=wmap)
        system.plot_fock(fig2, ax2[0])
        print("Evolution Q", time.time()-START_TIME)
        system.update('q')
        print("Plot. Time:", time.time()-START_TIME)
        print(system.effective_squeezing())
        system.plot(ax1[1], wmap=wmap)
        system.plot_fock(fig2, ax2[1])
        print("Evolution P", time.time()-START_TIME)
        system.update('p')
        print("Plot. Time:", time.time()-START_TIME)
        print(system.effective_squeezing())
        system.plot(ax1[2], wmap=wmap)
        system.plot_fock(fig2, ax2[2])
        print("Done", time.time()-START_TIME)
        fig1.tight_layout()
        fig1.savefig('wigner.pdf')
        fig2.tight_layout()
        fig2.savefig('fock.pdf')
        
    elif True:
        print("Start", time.time()-START_TIME)
        system = System(params)
        print(system.effective_squeezing())

        fig1, ax1 = plt.subplots(1, 2, figsize=(8,4))
        fig2, ax2 = plt.subplots(1, 2, figsize=(6,3))
        system.plot(ax1, husimi=False, wmap=wmap)
        system.plot_fock(fig2, ax2)
        fig1.tight_layout()
        fig1.savefig('wigner_0.png')
        fig2.tight_layout()
        fig2.savefig('fock_0.png')

        print("Evolution Q", time.time()-START_TIME)
        system.update('q')
        print("Plot. Time:", time.time()-START_TIME)
        print(system.effective_squeezing())

        system.displace(1j*3*np.sqrt(2*np.pi))
        
        fig1, ax1 = plt.subplots(1, 2, figsize=(8,4))
        fig2, ax2 = plt.subplots(1, 2, figsize=(6,3))
        system.plot(ax1, husimi=False, wmap=wmap)
        system.plot_fock(fig2, ax2)
        fig1.tight_layout()
        fig1.savefig('wigner_q.png')
        fig2.tight_layout()
        fig2.savefig('fock_q.png')
