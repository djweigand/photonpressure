import time

import matplotlib
import matplotlib.pyplot as plt
import numpy as np

import functions
from functions import (effective_squeezing, heterodyne, post_measurement,
                       qfunc_amat, System, Params, low_priority, parallel_map)

import os
import psutil

if __name__ == "__main__":
    low_priority()

    matplotlib.rcParams['mathtext.fontset'] = 'cm'
    matplotlib.rc('text', usetex=True)

    START_TIME = time.time()

    import os
    #data_dir = 'data/'
    data_dir = 'python/data/'
    assert os.path.exists(data_dir)

    # """Settings """
    purity = False

    """System parameters """
    #params = Params(0.1 * np.sqrt(2 * np.pi))
    params = Params("GKP")
    params.method = 'MC'
    params.dims = (200, 20)

    params.measurement = 'fixed'
    params.ntraj = 100
    params.initial_T = ('coherent', 0)

    loss_rates = np.array([0, 0.0001, 0.001, 0.01, 0.1, 1])

    params.kappa_A = 0.0001
    params.kappa_T = 0

    #loss_rates = np.array([0.01, 0.1, 1])
    params.ancilla_photons = 3
    """Initialize """
    print(time.time()-START_TIME)
    system = System(params)

    fig3, ax3 = plt.subplots(1, 4, figsize=(12, 4))
    system = System(params)

    # Simulate
    print("Start", time.time()-START_TIME)
    print("Purity", system.purity())
    print("Photons",system.photons())
    print("Squeezing", system.effective_squeezing())
    print("Plot Target 0, Time:", time.time()-START_TIME)
    #system.plot_line(fig3, ax3[0], "Target", wmap=True, title="Target (Initial)")
    print("Plot Ancilla 0, Time:", time.time()-START_TIME)
    #system.plot_line(fig3, ax3[1], "Ancilla", wmap=True, title="Ancilla (Initial)")
    print("Evolution Q", time.time()-START_TIME)
    system.update('q')
    print("Plot Husimi, Time:", time.time()-START_TIME)
    system.plot_line(fig3, ax3[2], "Husimi", wmap=True)
    print("Plot Target 1, Time:", time.time()-START_TIME)
    system.plot_line(fig3, ax3[3], "Target", wmap=True, title="Target (Final)")
    print("Done", time.time()-START_TIME)
    print("Purity", system.purity())
    print("Photons", system.photons())
    print(system.effective_squeezing())
    fig3.tight_layout(pad=0.3, w_pad=0, h_pad=0)
    fig3.savefig(f'figures/wigner_squeeze.pdf', bbox_inches='tight', pad_inches=0)
