import concurrent.futures
import functools
import pickle
import time
import warnings
from typing import List, Tuple

import matplotlib
import matplotlib.pyplot as plt
import numpy as np
import qutip
from mpl_toolkits.axes_grid1 import make_axes_locatable
from qutip import (basis, coherent, destroy, displace, expect,
                   ket2dm, liouvillian, mcsolve, mesolve, operator_to_vector,
                   plot_fock_distribution, qeye, qfunc, squeeze, tensor,
                   thermal_dm, vector_to_operator, wigner, wigner_cmap)

import functions
from functions import (effective_squeezing, heterodyne, post_measurement,
                       qfunc_amat, System, Params, low_priority, parallel_map)



if __name__ == "__main__":
    low_priority()

    matplotlib.rcParams['mathtext.fontset'] = 'cm'
    matplotlib.rc('text', usetex=True)


    START_TIME = time.time()

    warnings.warn("There is a bug in MKL crashing this script, use other BLAS versions of numpy/scipy. Check version using np.__config__.show()")
    import os
    data_dir = 'data/'
    #data_dir = 'python/data/'
    assert os.path.exists(data_dir)

    print(os.getcwd())

    plot=True

    """Settings """
    purity = False
    photons = False

    plot = "Kerr"
    debug_figs = False
    filetype = "pdf"
    print_p = True
    figname = 'gkp_pl_pq'

    load_data = True
    save_data = False
    save_new = False




    """System parameters """
    #params = Params(0.1 * np.sqrt(2 * np.pi))
    params = Params("GKP")
    params.nr_samples = 200
    params.n_range = np.arange(1,3.5,0.5)
    #params.n_range = np.arange(2.5, 3.5, 0.5)
    params.dims = (200, 20)

    params.measurement = 'random'
    params.ntraj = 500
    params.initial_T = ('coherent', 0)

    #loss_rates = np.array([0, 0.0001, 0.001, 0.01, 0.1, 1])
    loss_rates = np.array([0, 0.01, 0.1, 1])
    #loss_rates = np.array([0.01])

    if save_data is True:
        print(str(loss_rates))
        #assert input("OVERWRITING saved data. Type \"OK\" to continue.") == "OK"
    elif load_data == False and save_data == False:
        pass
        #assert input("Generating new data WITHOUT SAVING. Type \"OK\" to continue.") == "OK"


    for rate in loss_rates:
        print("rate=", rate, "time=", time.time()-START_TIME)

        if rate ==0:
            params.method = 'Lindblad'
        else:
            params.method = 'MC'

        data_name = f"vac_gkp_pl_{rate}"

        dq = []
        dp = []
        dq_err = []
        dp_err = []
        full_q = []
        full_p = []

        if rate == 0:
            params.kappa_A = None
        else:
            params.kappa_A = rate
            params.kappa_T = 0

        if save_new:
            if os.path.isfile(data_dir+data_name):
                load_data = True
            else:
                load_data = False

        if load_data:
            assert  os.path.isfile(data_dir+data_name)
        else:
            data = {"params": params}
            dat = []
            for n in params.n_range:
                params.ancilla_photons = n
                """Initialize """
                print("n=", n, "time=", time.time()-START_TIME)
                system = System(params)
                print("Squeezing", system.effective_squeezing())

                """Simulate """
                if params.measurement == 'fixed':
                    system.update('q')
                    print("meas done, time=", time.time()-START_TIME)
                    q, p = system.result
                    print("photon nr, time=", time.time()-START_TIME)
                    na = system.photons()[0]
                    print("Squeezing, time=", time.time()-START_TIME)
                    delta_p, delta_q = system.effective_squeezing() # pylint: disable=unbalanced-tuple-unpacking
                    out = [[delta_p, delta_q, na, (q+ 1j*p)/np.sqrt(2)],[delta_p, delta_q, na, (q+ 1j*p)/np.sqrt(2)]]
                else:
                    system.pre_measurement('q')
                    print("pre meas done, time=", time.time()-START_TIME)
                    #out = parallel_map(system.sample, np.arange(params.nr_samples), chunksize=int(params.nr_samples/16), max_workers=5)
                    out = parallel_map(system.sample, np.arange(params.nr_samples))
                    #out = [system.sample(idx) for idx in np.arange(params.nr_samples)]
                dat.append(out)
            data["data"] = np.real_if_close(np.array(dat))

            if save_data:
                with open(data_dir+data_name, "wb") as file:
                    pickle.dump(data, file, protocol=-1)

    lines = []
    labels = []
    #fig, ax = plt.subplots(1, 1, figsize=(3.4, 3.4))
    fig, ax = plt.subplots(1, 1, figsize=(5, 5))
    colors = plt.cm.viridis(np.linspace(0, 1, len(loss_rates)))    # pylint: disable=no-member
    for i, rate in enumerate(loss_rates):
        data_name = f"vac_gkp_pl_{rate}"
        with open(data_dir+data_name, "rb") as file:
            data = pickle.load(file)

        dat = data["data"]
        x = data["params"].n_range

        mean = np.mean(dat, axis=1)
        std = np.std(dat, axis=1)

        if rate == 0:
            print(data["params"].kappa_A)
            print(dat)
            print(np.real_if_close(mean[:,0]))
            print(np.real_if_close(std[:,0]))
            print(np.real_if_close(mean[:,1]))
            print(np.real_if_close(std[:,1]))

        (line,_,_) = ax.errorbar(x, np.real_if_close(mean[:,1]), yerr=np.real_if_close(std[:,1]), label=str(rate), color=colors[i], capsize=6, linestyle='-')
        if print_p:
            ax.errorbar(x, np.real_if_close(mean[:,0]), yerr=np.real_if_close(std[:,0]), label=str(rate), color=colors[i], capsize=6, linestyle='--')

        lines.append(line)
        labels.append(str(rate))

    x_analytic = np.arange(1, 10.5, 0.5)
    ones = np.ones_like(x_analytic)
    #ax.plot(x_analytic, ones)
    ax.set_ylim(0, 2)
    plt.legend(lines, labels)
    fig.tight_layout()
    fig.savefig(f"figures/{figname}.{filetype}",bbox_inches='tight', pad_inches=0)
