import concurrent.futures
import functools
import pickle
import time
import warnings
from typing import List, Tuple

import matplotlib
import matplotlib.pyplot as plt
import numpy as np
import qutip
from mpl_toolkits.axes_grid1 import make_axes_locatable
from qutip import (basis, coherent, destroy, displace, expect,
                   ket2dm, liouvillian, mcsolve, mesolve, operator_to_vector,
                   plot_fock_distribution, qeye, qfunc, squeeze, tensor,
                   thermal_dm, vector_to_operator, wigner, wigner_cmap)

import functions
from functions import (effective_squeezing, heterodyne, post_measurement,
                       qfunc_amat, Samples, Params, low_priority, parallel_map)



if __name__ == "__main__":
    low_priority()

    font_size = 20
    matplotlib.rcParams['mathtext.fontset'] = 'cm'
    matplotlib.rc('text', usetex=True)
    matplotlib.rcParams.update({'font.size': font_size})
    matplotlib.rcParams['mathtext.fontset'] = 'cm'
    matplotlib.rcParams['axes.labelsize'] = font_size
    matplotlib.rc('text', usetex=True)
    matplotlib.rcParams['xtick.labelsize'] = font_size
    matplotlib.rcParams['ytick.labelsize'] = font_size
    matplotlib.rcParams['legend.fontsize'] = font_size


    START_TIME = time.time()
    warnings.warn("Needs to be run with anaconda prompt. VScode has ModuleNotFoundError")

    warnings.warn("There is a bug in MKL crashing this script, use other BLAS versions of numpy/scipy. Check version using np.__config__.show()")
    import os
    data_dir = 'data/'
    #data_dir = 'python/data/'
    assert os.path.exists(data_dir)

    print(os.getcwd())

    plot=True

    """Settings """
    purity = False
    photons = False

    plot = "Kerr"
    debug_figs = False
    filetype = "svg"

    load_data = True
    save_data = False


    """System parameters """
    params = Params('GKP')
    params.nr_samples = 200
    params.n_range = np.arange(1,4.5,0.5)
    #params.n_range = np.arange(1,2,0.5)
    params.dims = (500, 20)

    params.measurement = 'random'
    params.method = 'Lindblad'
    params.initial_T = ('coherent', 0)

    params.time_dependent = True
    drive_freq0 = 2*np.pi * 2 / (2 * np.sqrt(np.pi))
    #drive_freqs = np.array([0, 1, 5.5, 10.5, 15.5, 16])
    drive_freqs = np.array([0, 50.5, 500.5])

    #kerr_types = ('target', 'cross', 'ancilla')
    kerr_types = ("full",)
    #drive_freqs = np.array([0.5])

    if save_data is True:
        print(str(drive_freqs)+str(kerr_types))
        assert input("OVERWRITING saved data. Type \"OK\" to continue.") == "OK"
    elif load_data == False and save_data == False:
        assert input("Generating new data WITHOUT SAVING. Type \"OK\" to continue.") == "OK"

    for kerr_type in kerr_types:
        params.kerr_type = kerr_type
        kerr_strength = [1, 0.01, 0.025]
        if kerr_type == 'ancilla':
            params.kerr_strength = kerr_strength[0]
        elif kerr_type == 'target':
            params.kerr_strength = kerr_strength[1]
        elif kerr_type == 'cross':
            params.kerr_strength = kerr_strength[2]
        elif kerr_type == 'full':
            params.kerr_strength = kerr_strength

        data_name_end = f"_vac_kerr_{params.kerr_type}"

        print(params.kerr_strength)
        print(params.kerr_type)

        for freq in drive_freqs:
            params.drive_freq = freq * drive_freq0

            dq = []
            dp = []
            dq_err = []
            dp_err = []
            full_q = []
            full_p = []

            if load_data:
                with open(data_dir+str(freq)+data_name_end, "rb") as file:
                    data = pickle.load(file)
            else:
                data = {"params": params}
                dat = []
                for n in params.n_range:
                    params.ancilla_photons = n
                    """Initialize """
                    print("n=", n, "time=", time.time()-START_TIME)
                    system = Samples(params)
                    print("Squeezing", system.effective_squeezing())

                    """Simulate """
                    if params.measurement == 'fixed':
                        system.update('q')
                        print("meas done, time=", time.time()-START_TIME)
                        q, p = system.result
                        print("photon nr, time=", time.time()-START_TIME)
                        na = system.photons()[0]
                        print("Squeezing, time=", time.time()-START_TIME)
                        delta_p, delta_q = system.effective_squeezing() # pylint: disable=unbalanced-tuple-unpacking
                        out = [[delta_p, delta_q, na, (q+ 1j*p)/np.sqrt(2)],[delta_p, delta_q, na, (q+ 1j*p)/np.sqrt(2)]]
                    else:
                        system.pre_measurement('q')
                        print("pre meas done, time=", time.time()-START_TIME)
                        out = parallel_map(system.sample, np.arange(params.nr_samples), chunksize=int(params.nr_samples/16))
                        #out = [system.sample(idx) for idx in np.arange(params.nr_samples)]
                    dat.append(out)
                data["data"] = np.real_if_close(np.array(dat))

                if save_data:
                    with open(data_dir+str(freq)+data_name_end, "wb") as file:
                        pickle.dump(data, file, protocol=-1)

        lines = []
        labels = ["Ideal", 50.5, 500.5]
        fig, ax = plt.subplots(1, 1, figsize=(5, 5))
        #colors = plt.cm.viridis(np.linspace(0,1,len(drive_freqs)))    # pylint: disable=no-member
        colors = plt.cm.viridis(np.linspace(0,1,4))
        for i, freq in enumerate(drive_freqs):
            with open(data_dir+str(freq)+data_name_end, "rb") as file:
                data = pickle.load(file)

            dat = data["data"]
            x = data["params"].n_range

            mean = np.mean(dat, axis=1)
            std = np.std(dat, axis=1)
            (line,_,_) = ax.errorbar(x, mean[:,1], yerr=std[:,1], label=str(freq),capsize=3,color=colors[i])
            lines.append(line)
            #labels.append(str(freq))

        #x_analytic = np.arange(1,10.5,0.5)
        #ones = np.ones_like(x_analytic)
        #ax.plot(x_analytic, ones)
        ax.set_ylim(0,0.5)
        ax.set_xticks([1,4])
        ax.set_yticks([0,0.25,0.5])
        ax.set_xlabel(r'$\bar{n}_A$')
        ax.set_ylabel(r'$\langle \Delta_q \rangle$')
        plt.legend(lines, labels, title=r"$\omega_T$ (Mhz)")
        fig.tight_layout()
        fig.savefig(f"figures/{data_name_end}.{filetype}",bbox_inches='tight', pad_inches=0)
